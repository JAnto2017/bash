# Administración de sistemas Linux

- [Administración de sistemas Linux](#administración-de-sistemas-linux)
  - [Consola Avanzada: Shell Script](#consola-avanzada-shell-script)
    - [Elementos del lenguaje de scripting](#elementos-del-lenguaje-de-scripting)
      - [Ejemplo script - Hola Mundo](#ejemplo-script---hola-mundo)
    - [Variables](#variables)
      - [Ejemplo script - Uso de variable](#ejemplo-script---uso-de-variable)
      - [Ejemplo script - Operaciones matemáticas](#ejemplo-script---operaciones-matemáticas)
    - [Desarrollo de Comandos](#desarrollo-de-comandos)
      - [Ejemplo script - Hora del OS y muestra la información](#ejemplo-script---hora-del-os-y-muestra-la-información)
    - [Interactuando con el Usuario: Parámetros de entrada](#interactuando-con-el-usuario-parámetros-de-entrada)
    - [Interactuando con el Usuario: Datos por Teclado](#interactuando-con-el-usuario-datos-por-teclado)
      - [Ejemplo script - Lectura por teclado](#ejemplo-script---lectura-por-teclado)
  - [Comandos Útiles para Scripting](#comandos-útiles-para-scripting)
    - [Imprimir Información: echo](#imprimir-información-echo)
      - [Ejemplo script - Uso de echo](#ejemplo-script---uso-de-echo)
      - [Ejemplo script - Uso de echo con otros programas](#ejemplo-script---uso-de-echo-con-otros-programas)
    - [¿Qué usuario soy? whoami](#qué-usuario-soy-whoami)
    - [Fecha y Hora del Sistema: date](#fecha-y-hora-del-sistema-date)
    - [Mostrando un Calendario: cal](#mostrando-un-calendario-cal)
    - [Traducción y Borrado de Caracteres: tr](#traducción-y-borrado-de-caracteres-tr)
      - [Ejemplo script - Uso de tr y |](#ejemplo-script---uso-de-tr-y-)
    - [Eliminando Información de una Línea: cut](#eliminando-información-de-una-línea-cut)

---

## Consola Avanzada: Shell Script

A través de la _shell_ se pueden construir secuencias de comandos de consola llamados _scripts_ que implementan las tareas que queremos realizar.

### Elementos del lenguaje de scripting

En un editor de texto, se escribe el _script_, líneas del lenguaje de programación a ejecutar. Para la primera línea de un _script_ se debe escribir: `#!/bin/bash`.

La explicación de ésta, primera línea, es indicar al sistema operativo que el programa debe ejecutarse por medio de la _shell_.

La convención de nombres de los _script_ es añadirle al nombre del _script_ **.sh** para hacer notar que se trata de un _script_. Después tendremos que dat derechos de ejecución al archivo según quien queremos que pueda ejecutarlo (propietario, grupo u otros) y tan solo queda invocarlo.

La forma de ejecutarlo (una vez que tenga los permisos de ejecución), si estamos en otro directorio distinto a donde está almacenado el _script_ lo invocamos como una ruta más. En caso de estar en el mismo directorio anteponemos "/" a la invocación del _script_.

#### Ejemplo script - Hola Mundo

Creamos un _script_ de ejemplo que muestre un mensaje por pantalla empleando el comando `echo`.

```bash
#!/bin/bash
echo "Hola mundo. Este es el primer script..."
```

Lo guardamos como `hola_mundo.sh` y le daremos permisos de ejecución para invocarlo: `chmod u+x hola_mundo.sh`.

Por último, sólo queda invocarlo ejecutando desde la línea de comando, encontrándonos dentro del directorio donde se encuentra el _script_: `./hola_mundo.sh`.

Si queremos añadir comentarios al _script_ que no serán ejecutados:

```bash
#!/bin/bash
# Esto es un comentario de texto
echo "Hola mundo. Este es el primer script..."
```

### Variables

Para definir una variable se necesita proporcionar un nombre y un valor que asignarle. Para asignar valores a las variables se emplea el operador asignación "=" (signo igual) `nombre_variable=valor_variable`.

No hay que dejar nigún espacio entre el nombre de la variable y el signo igual (=), ya que de existir espacio, el intérprete de comandos tomará el nombre de la variable como un comando y el símbolo igual como un parámetro del comando, mostrando el error típico de comando no encontrado.

```bash
# creamos la variable mensaje
hola="Hola Mundo"
# Obtener el valor de una variable anteponiendo $ al nombre de la variable
$hola
# Asignar el valor de una variable a otra
var2=$var1
```

A la hora de asignar valores a las variables hemos de tener en cuenta el tipo de dato que estamos asignando, pues no se pueden realizar cálculos con textos o valores numéricos a la vez.

Los tipos de datos que se pueden definir son:

- **Texto**, el valor va encerrado entre comillas dobles.
- **Numérico**, el valor se indica tal cual.

#### Ejemplo script - Uso de variable

En el siguiente _script_, `mensajes.sh` se ha definido una variable y mostrarla por pantalla:

```bash
#!/bin/bash
mensaje="Hola Mundo"
echo $mensaje
```

#### Ejemplo script - Operaciones matemáticas

Con las variables se pueden realizar operaciones matemáticas:

```bash
#!/bin/bash
var1=5
var2=9
resul=0
resul=$var1+$var2
echo "SUMA"
echo $resul
resul=$var1-$var2
echo "RESTA"
echo $resul
resul=$var1*$var2
echo "PRODUCTO"
echo $resul
resul=$var1/$var2
echo "DIVISION"
echo $resul
```

### Desarrollo de Comandos

En situaciones en las que necesitamos utilizar la salida de un comando como parámetro de entrada de otro comando, esto se realiza usando el operador "\|".

Para conseguir esta funcionalidad, en el _shell_ de _bash_ se permite el despliegue de comandos de dos formas con una muy ligera diferencia:

- Encerrado el comando entre la comilla francesa de la siguiente forma: \`comando\`.
- Utilizándolo como una variable de entorno y encerrándolo entre paréntesis de la siguiente forma: `$(comando)`.

Ambas formas son válidas, para expandir en el lugar donde se utiliza el comando, el resultado de ejecutar dicho comando. Por ejemplo, si utilizamos el comando _whoami_ obtendremos como resultado de su ejecución el nombre del usuario que ha ejecutado ese comando, si ese resultado lo tratáramos como parámetro de entrada sobre el comando _ls_, podremos listar el contenido del directorio _home_ del usuario actual ejecutando el siguiente comando: `ls /home/$(whoami)`.

La utilización de la expansión del resultado de comandos no se limita a la línea de comandos. En los _scripts_ de _shell_ es donde mejor se puede aprovechar mejor esta funcionalidad, ya que es posible recoger en una variable la información devuelta por un comando y utilizarlo como método de entrada para otro.

#### Ejemplo script - Hora del OS y muestra la información

Este _script_ recoge la hora del sistema y se muestra su salida por pantalla:

```bash
#!/bin/bash
hora=`date+%H`
echo $hora
```

### Interactuando con el Usuario: Parámetros de entrada

Un _script_, al igual que los comandos de _shell_, tiene la posibilidad de recibir parámetros a la hora de ser invocado, para ello invocaremos al _script_ poniendo a continuación de su nombre todos los parámetros que admita, separados por espacios en blanco: `./scrip.sh param1 param2 ... paramN`.

Dichas variables son el número de la posición del parámetro que hemos introducido, para utilizarlas en nuestro _script_, las precederemos con el símbolo **$**.

La variable **0** se reserva para la cadena que hemos introducido a la hora de referirnos al script para su ejecución.

En la variable **#** se guarda el número de parámetros que hemos pasado al _script_.

En el ejemplo anteriormente comentado se realizará la siguiente carga de parámetros:

```bash
0=./script.sh
1=param1
2=param2
3=param3
4=...
5=paramN
#=5
```

Si intentamos desplegar la variable _$10_, no obtendremos el valor del décimo parámetro introducido a la hora de invocar el _script_, sino que obtendremos el valor del primer parámetro concatenado con un _0_. Para utilizar los parámetros que quedan por encima del _9_, debemos encerrar el oridinal a utilizar entre llaves _{}_, de esta forma, para desplegar el décimo parámetro utilizaremos la sentencia _\${10}_.

Como alternativa podremos utilizar la directiva _shift_ dentro de nuestro _script_. Esta directiva desplaza todos los parámetros una posición por debajo de la que ocupan, perdiendo el valor del parámetro _$1_, tras cada invocación de esta directiva.

La forma que se recomienda, para la utilización de los parámetros que le llegan al _script_, es la nomenclatura entre _{}_.

### Interactuando con el Usuario: Datos por Teclado

Para que los _script_ pueda interactura con el usuario, por ejemplo, en la selección de un menú o en la confirmación de una acción crítica.

Para solicitar la entrada de datos por teclado por parte del usuario, se emplea la instrucción _read_ seguida del nombre de la variable donde queremos almacenar la información. La función _read_ estará aceptando caracteres hasta que se pulse la tecla _INTRO_.

#### Ejemplo script - Lectura por teclado

```bash
#!/bin/bash
echo "¿Cómo te llamas?"
read nombre
echo "Te llamas "$nombre
```

Al ejecutar este _script_ el sistema nos hace la pregunta y espera que el usuario introduzca información por teclado. Al pulsar la tecla _INTRO_ se devuelve el nombre teclado. En la última sentencia del _script_ cómo se ha mezclado una cadena de texto con una variable.

## Comandos Útiles para Scripting

### Imprimir Información: echo

El comando **echo** realiza una impresión por pantalla de la cadena de texto que se le pasa como argumento. La sintaxis del comando es: `echo <text>`.

#### Ejemplo script - Uso de echo

```bash
echo "Hola, soy un usuario de los GNU/Linux"
```

En este ejemplo, se mostrará por pantalla el mensaje indicado entre comillas dobles.

#### Ejemplo script - Uso de echo con otros programas

Es posible mezclar texto con la salida de otros programas expandiendo su ejecución de cualquiera de las formas que hemos visto:

```bash
echo "La hora es: `date +%H:%M:%S`"
```

Muestra el mensaje _la hora es: 22:10:00"_

### ¿Qué usuario soy? whoami

El comando **whoami** informa sobre el nombre del usuario que está ejecutando dicho comando.

### Fecha y Hora del Sistema: date

Por medio del comando **date** podemos obtener la fecha y la hora del sistema o incluso modificar estos valores si tenemos los privilegios del usuario _root_. Esto es debido a que, hay muchos procesos que son muy sensisbles a la temporización del sistema.

La introducción de la fecha y la hora es de forma manual, tecleándola. Para hacerlo de forma automática, se debe recurrir a un servidor de hora y cada cierto tiempo, sincronizarse con el servidor para tener siempre la fecha y hora más exacta.

La sintaxis puede ser la siguiente: `date [opción...][+FORMAT]`, `date [opción][MMDDhhmm[[CC]AA][.ss]]`.

El comando **date** dispone de muchos modificadores que alteran la presencia de la _fecha_ y la _hora_. Una lista completa de todos los modificadores se puede obtener desde su manual **man date**:

| Modificador | Descripción |
| :---: | :--- |
| %a | día de la semana en formato abreviado, por ejemplo: L. |
| %A | día de la semana en formato completo, por ejemplo: Lunes. |
| %b | nombre del mes abreviado, por ejemplo: En. |
| %b | nombre del mes completo, por ejemplo: Enero. |
| %d | día del mes, por ejemplo: 1. |
| %m | número de mes, por ejemplo: 2. |
| %H | presenta la hora en formato de 24 horas. |
| %M | obtiene los minutos de la hora. |
| %S | obtiene los segundos de la hora. |
| | |

### Mostrando un Calendario: cal

El comando **cal** muestra por pantalla un calendario en función de la fecha que le indiquemos como parámetros, si no indicamos ningún parámetro, el sistema mostrará el calendario del mes actual.

`cal [-jy][[mes] año]`

Los parámetros que aceptan son:

- **-j**: mostrar el calendario en formato juliano.
- **-y**: muestra un calendario con todos los meses del año actual.

### Traducción y Borrado de Caracteres: tr

En ocasiones, la información que se nos muestra por pantalla es excesiva, para solucionar este problema contamos con el comando **tr**, el cual nos permite modificar la información o borrar aquella información que no nos sea útil.

Su formato es el siguiente: `tr [opcional] conjunto1 [conjunto2]`.

Las opciones más destacables de este comando son las siguientes:

- **-d**: borra los caracteres que se especifiquen como _conjunto1_.
- **-s**: sustituye cada uno de los caracteres del _conjunto1_ por los que se especifiquen en el _conjunto2_. El orden en el que se escriben los diferentes conjuntos es determinante para saber que sustitución se realiza.

Los conjuntos son cadenas de caracteres, aunque también se encuentran disponibles una serie de cadenas especiales que representan conjuntos concretos de información; los más significativos son:

| Modificador | Descripción |
| :--: | :-- |
| **[:allnum:]** | Conjunto que contiene todas las letras del alfabeto y números. |
| **[:blank:]** | Espacios en blanco y tabuladores. |
| **[:lower:]** | Todas las letras minúsculas. |
| **[:upper:]** | Todas las letras mayúsculas. |
| **[:digit:]** | Todos los números. |
| **[:punct:]** | Todos los símbolos de puntuación. |
| | |

Podemos utilizar varios conjuntos a la vez, separándolos por comas ",".

#### Ejemplo script - Uso de tr y |

Podemos poner todo el texto de entrada de un fichero, o salida en mayúsculas, haciendo una sustitución poniendo como _conjunto1_, el conjunto de letras en minúsculas, y como _conjunto2_ el conjunto de letras mayúsculas.

```bash
man tr | tr -s [:lower:] [:upper:]
```

Podemos eliminar todos los números y guiones de un flujo de caracteres de la siguiente forma:

```bash
ls -l | tr -d [:digit:],-
```

### Eliminando Información de una Línea: cut