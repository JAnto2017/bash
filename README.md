# CURSO BASH - TALLER SHELL SCRIPT

- [CURSO BASH - TALLER SHELL SCRIPT](#curso-bash---taller-shell-script)
  - [Sección 1 - Contenido iniciales](#sección-1---contenido-iniciales)
    - [Primer shell script](#primer-shell-script)
    - [Diferentes maneras de invocar un Shell Script](#diferentes-maneras-de-invocar-un-shell-script)
    - [Diferentes Shells](#diferentes-shells)
    - [Variables en Shell Script](#variables-en-shell-script)
    - [Ciclos FOR en Shell Script](#ciclos-for-en-shell-script)
    - [Framework Identify](#framework-identify)
  - [Seccion 2 - Condicionales y comprobaciones en Shell Script](#seccion-2---condicionales-y-comprobaciones-en-shell-script)
    - [Condicionales](#condicionales)
  - [Sección 3 - Cadenas de texto](#sección-3---cadenas-de-texto)
    - [Concatenar cadenas en Shell Script](#concatenar-cadenas-en-shell-script)
    - [Mostrar cadenas unidas con echo en Shell Script](#mostrar-cadenas-unidas-con-echo-en-shell-script)
    - [Extraer subcadenas en Shell Script](#extraer-subcadenas-en-shell-script)
  - [Sección 4 - Comparar números](#sección-4---comparar-números)
    - [Aleatoriedad con shuf](#aleatoriedad-con-shuf)
    - [Comparar números en Shell Script](#comparar-números-en-shell-script)
  - [Sección 5 - Estructuras While y Until](#sección-5---estructuras-while-y-until)
    - [Estructura While\<](#estructura-while)
    - [Estructura Until](#estructura-until)
  - [Sección 6 - Leer datos por teclado](#sección-6---leer-datos-por-teclado)
    - [Leer datos por teclado en Shell Script](#leer-datos-por-teclado-en-shell-script)
  - [Sección 7 - Mejorando los scripts](#sección-7---mejorando-los-scripts)
    - [Uso de exit en shell script](#uso-de-exit-en-shell-script)
    - [Redirecciones de comandos I](#redirecciones-de-comandos-i)
    - [Redirecciones de comandos II](#redirecciones-de-comandos-ii)
    - [Redirecciones de error](#redirecciones-de-error)
    - [Ejemplos redirecciones de error](#ejemplos-redirecciones-de-error)
    - [Internacionalizar mensajes](#internacionalizar-mensajes)
    - [Parámetros posicionales en Shell Script](#parámetros-posicionales-en-shell-script)
  - [Sección 8 - Funciones](#sección-8---funciones)
    - [Funciones en Shell Script](#funciones-en-shell-script)
    - [Funciones sin parámetros ni retornos en Shell Script](#funciones-sin-parámetros-ni-retornos-en-shell-script)
    - [Funciones con parámetros en Shell Script](#funciones-con-parámetros-en-shell-script)
    - [Funciones con parámetros en scripts con parámetros](#funciones-con-parámetros-en-scripts-con-parámetros)
    - [Recibir en variables el resultado de funciones](#recibir-en-variables-el-resultado-de-funciones)
  - [Sección 9 - Operaciones Matemáticas](#sección-9---operaciones-matemáticas)
    - [Operaciones matemáticas con doble paréntesis](#operaciones-matemáticas-con-doble-paréntesis)
    - [Incremento y decremento con doble paréntesis](#incremento-y-decremento-con-doble-paréntesis)
    - [El orden de operaciones PEMDAS](#el-orden-de-operaciones-pemdas)
    - [Operaciones con Let](#operaciones-con-let)
    - [Operar y asignar con let y dobles paréntesis](#operar-y-asignar-con-let-y-dobles-paréntesis)
    - [Operaciones matemáticas con expr](#operaciones-matemáticas-con-expr)
    - [Operaciones matemáticas con bc](#operaciones-matemáticas-con-bc)
    - [Cambio sistema de numeración con bc](#cambio-sistema-de-numeración-con-bc)
    - [Funciones matemáticas de bc](#funciones-matemáticas-de-bc)
  - [Sección 10 - Bibliotecas](#sección-10---bibliotecas)
    - [Ficheros externos con funciones](#ficheros-externos-con-funciones)
    - [Comentarios y documentación de scripts y funciones](#comentarios-y-documentación-de-scripts-y-funciones)
    - [Ejemplo de comentarios y documentación de funciones](#ejemplo-de-comentarios-y-documentación-de-funciones)
    - [Sustituir un carácter](#sustituir-un-carácter)
    - [Función multiplicar](#función-multiplicar)
    - [Función lista de números y resta](#función-lista-de-números-y-resta)
    - [Función Máximo número de decimales](#función-máximo-número-de-decimales)
    - [Función con decimales](#función-con-decimales)
    - [Función Potencia](#función-potencia)
    - [Función mayor de los números](#función-mayor-de-los-números)
    - [Función menor de los números](#función-menor-de-los-números)
    - [Función número aleatorio](#función-número-aleatorio)
  - [Sección 11 - Trabajar con ficheros](#sección-11---trabajar-con-ficheros)
    - [Recorrer ficheros Shell Script](#recorrer-ficheros-shell-script)
    - [Comprobar ficheros en Shell Script](#comprobar-ficheros-en-shell-script)
  - [Sección 12 - Estructura Case](#sección-12---estructura-case)
    - [Estructura case en Shell Script I](#estructura-case-en-shell-script-i)
    - [Estructura case en Shell Script II](#estructura-case-en-shell-script-ii)
    - [Estructura case en Shell Script III](#estructura-case-en-shell-script-iii)
    - [Estructura case en Shell Script IV](#estructura-case-en-shell-script-iv)
    - [Uso de select y case en Shell Script](#uso-de-select-y-case-en-shell-script)
    - [Uso de case con parámetros no posicionales en Shell Script](#uso-de-case-con-parámetros-no-posicionales-en-shell-script)
    - [Uso de arrays en Shell Script I](#uso-de-arrays-en-shell-script-i)
    - [Uso de arrays en Shell Script II](#uso-de-arrays-en-shell-script-ii)
  - [Sección 13 - Variables de entorno y depuración de scripts](#sección-13---variables-de-entorno-y-depuración-de-scripts)
    - [Uso de las variables de entorno en Shell Script](#uso-de-las-variables-de-entorno-en-shell-script)
    - [Depuración en Shell Script](#depuración-en-shell-script)
  - [Sección 14 - Proyectos crear ficheros y trabajar con imágenes](#sección-14---proyectos-crear-ficheros-y-trabajar-con-imágenes)
    - [Presentación segunda parte del curso](#presentación-segunda-parte-del-curso)
    - [Enunciado ejercicio crear fichero](#enunciado-ejercicio-crear-fichero)
    - [Resolución ejercicio crear fichero I](#resolución-ejercicio-crear-fichero-i)
    - [Resolución ejercicio crear fichero II](#resolución-ejercicio-crear-fichero-ii)
    - [Resolución ejercicio crear fichero III](#resolución-ejercicio-crear-fichero-iii)
    - [Anotaciones para los ejecicios](#anotaciones-para-los-ejecicios)
    - [Anotaciones dvi](#anotaciones-dvi)
    - [Anotaciones funciones I](#anotaciones-funciones-i)
    - [Anotaciones funciones II](#anotaciones-funciones-ii)
  - [Sección 15 - Uso de gettext](#sección-15---uso-de-gettext)
    - [Uso de gettext I](#uso-de-gettext-i)
    - [Uso de gettext II](#uso-de-gettext-ii)
    - [Uso de gettext III](#uso-de-gettext-iii)
  - [Guardar extensión](#guardar-extensión)
    - [Enunciado ejercicio Guardar Extensión](#enunciado-ejercicio-guardar-extensión)
    - [Ejercicio "guardar extensión"](#ejercicio-guardar-extensión)
    - [Ejercicio "test guardar texto en fichero" I](#ejercicio-test-guardar-texto-en-fichero-i)
    - [Ejercicio "test guardar texto en fichero" II](#ejercicio-test-guardar-texto-en-fichero-ii)
    - [Ejercicio "test guardar texto en fichero" III](#ejercicio-test-guardar-texto-en-fichero-iii)
    - [Ejercicio "test guardar texto en fichero" IV](#ejercicio-test-guardar-texto-en-fichero-iv)
    - [Ejercicio "leer contenido de un fichero"](#ejercicio-leer-contenido-de-un-fichero)
    - [Ejercicio "test función devuelve resultado esperado"](#ejercicio-test-función-devuelve-resultado-esperado)
    - [Función que permite modificar el lenguaje del proyecto I](#función-que-permite-modificar-el-lenguaje-del-proyecto-i)
  - [Uso del comando grep](#uso-del-comando-grep)
    - [Introducción al comando grep](#introducción-al-comando-grep)
    - [Modificador grep I](#modificador-grep-i)
    - [Modificador grep II](#modificador-grep-ii)
    - [Modificador grep para extraer líneas](#modificador-grep-para-extraer-líneas)
    - [Modificador grep para búsquedas en múltiples ficheros](#modificador-grep-para-búsquedas-en-múltiples-ficheros)
  - [Expresiones regulares](#expresiones-regulares)
    - [Introducción a las expesiones regulares](#introducción-a-las-expesiones-regulares)
    - [Uso de expresiones regulares básicas - caracteres y punto](#uso-de-expresiones-regulares-básicas---caracteres-y-punto)
    - [Uso de expresiones regulares básicas - rangos](#uso-de-expresiones-regulares-básicas---rangos)
    - [Uso de expresiones regulares - corchetes](#uso-de-expresiones-regulares---corchetes)
    - [Uso de expresiones regulares - clases de caracteres](#uso-de-expresiones-regulares---clases-de-caracteres)
    - [Uso de expresiones regulares - cuantificadores](#uso-de-expresiones-regulares---cuantificadores)
    - [Uso de expresiones regulares - agrupaciones](#uso-de-expresiones-regulares---agrupaciones)
    - [Uso de expresiones regulares - alternativas](#uso-de-expresiones-regulares---alternativas)
    - [Uso de expresiones regulares - comienzos y finales](#uso-de-expresiones-regulares---comienzos-y-finales)
  - [Uso de AWK](#uso-de-awk)
    - [Introducción a awk](#introducción-a-awk)
    - [Uso de awk con NR](#uso-de-awk-con-nr)

---

## Sección 1 - Contenido iniciales

### Primer shell script

Hola mundo, en **Shell Script**
El archivo debe tener extensión **.sh**

En el editor, se debe usar al inicio del archivo la siguiente línea de código: `#!/bin/bash`

Para dar permisos de ejecución al archivo *.sh* se debe ejecutar: `chmod +x holamundo.sh`

### Diferentes maneras de invocar un Shell Script

Sin poner la extensión **.sh** en el fichero, se puede ejecutar y funciona. Si que debe tener los permisos de ejecución.

Para ejecutar los comandos, sin tener que escribir el `./`, los archivos deben estar en un directorio *PATH* del sistema.

Para saber los directorios disponibles se puede ejecutar: `echo $PATH`
Las variables de entorno en Ubuntu, pueden estar en: `vi ~/.bashrc` ó en `vi ~/.profile`. En sistemas W10 las variables de entorno no utilizan los archivos antes indicados.

Al añadir `mkdir ~/bin` `source ~/.profile` y comprobando con `echo $PATH` todos los archivos que coloquemos dentro de *~/bin* en sistemas Ubuntu, se podrán ejecutar sin la necesidad del `./`

### Diferentes Shells

Para conocer las *Shell* con la que estamos trabando, ejecutar: `echo $SHELL` (es lo mismo que ejecutar `which bash`).
Devuelve: `/usr/bin/bash`.

Con el comando: `cat /etc/shells`, nos muestra los *Shells* válidos.
Se puede cambiar el 'shivrang' (primera línea de código en los archivos.sh) y poner, por ejemplo: `#!/usr/bin/bash`.

### Variables en Shell Script

Al crear una variable se utilizan letras, sin comenzar por número o símbolo; por ejemplo: *cadena="Hola mundo"*. No se deben dejar espcios entre nombre de la variable el igual y el valor de la variable.

Para acceder a la variable si que se debe usar el símbolo de dolar delante del nombre de la variable *$cadena*.

Se puede almacenar en una variable, el resultado de la ejecución de los comando, por ejemplo: `listado=$(ls /bin)`. Almacena en la variable, el listado disponible en el directorio.

A las variables, se le pueden pasar operaciones matemáticas, por ejemplo: `resultado=$(( $num1 + $num2 ))`

### Ciclos FOR en Shell Script

El ciclo **for** con una lista: `for color in rojo verde azul`.
Otra forma de hacer los ciclos **for** en lugar de listas, es pasando un rango: `for i in {1..9}`.

También es posible indicar el paso o saltos en la iteración. Se indica al final después de los dos puntos, ejemplo: `for i in {1..9..2}`. Produce saltos de dos en dos.

Trabajando con el comando **seq** (secuencia) en los ciclos **for**: `for i in $(seq 5 11)`.

Si quisiéramos saltos en la sequencia, el comando quedaría de la forma: `for i in $(seq 5 2 11)` Dónde el 2 indica el paso y se coloca en medio de la terna de números.

Los ciclos **for** trabajando con *tuberías*: `for i in $(seq 0 .2 2 | tr , .)` Sustituye el carácter (coma) por el carácter (punto). La tubería se indica con **|** y con **tr** sustituye los caracteres.

### Framework Identify

Uso de un Framework: **identify imagen.png**. Devuelve tamaño y otros datos de la imagen.

En Ubuntu, usando el comando:

1. **man identify**. Muestra la ayuda.
2. **identify -format %w imagen.png**. Indica el ancho en píxel.
3. **identify -format %h imagen.png**. Indica la altura en píxel.

---

## Seccion 2 - Condicionales y comprobaciones en Shell Script

### Condicionales

Uso del condicional *if [ ] then*, permite comprobar si se cumple o no, una condición. Utiliza los corchetes con separación. Para finalizar el condicional se pone *fi*.

> `if [ condición ]`
> `then`
> se ejecuta este código si es verdadero la condición
> `else`
> se ejecuta este código si no es verdadero la condición
> `fi`

En la condición de igualdad, es más correcto utilizar un solo igual `=` que los dos iguales de comparación clásico en otros lenguajes de programación `==`. En caso de usar los dos iguales, se deben poner doble corchete. En Ubuntu funciona bien el doble igual y el corchete único para la apertura y el cierre.

Para la comprobación del distinto, se debe usar `!=`.

Para comprobar si una cadena contiene otra cadena de texto, se debe utilizar `=~`. Cuando se utiliza este tipo de comparador, para evitar el error del *.bash* se debe utilizar el doble corchete para la apertura y para el cierre en el **if [[ ]]**.

---

## Sección 3 - Cadenas de texto

### Concatenar cadenas en Shell Script

Las variables de texto, se pueden concatenar simplemente poniendo una detrás de otra. Recordando poner el símbolo de dolar `$` antes del nombre de la variable.

También es posible combinar variables con comillas dobles (literales), realizando una concatenación combinada.

### Mostrar cadenas unidas con echo en Shell Script

Con la instrucción `echo` + nombre de la variable, se ejecuta y representa el contenido de la variable en una línea (se produce salto de línea tras su ejecución).  

Para que se representen los textos concatenados, se debe utilizar el *modificador* `-n` y las variables entrecomilladas, quedando de la siguiente forma:
`echo -n "$variable1"`  
`echo -n "$variable2"`  

El comando `sleep` retarda un tiempo en segundos.  

### Extraer subcadenas en Shell Script

Para subdividir una cadena de texto en palabras, se debe conocer la longitud de la cadena, por ejemplo: `longitud=${#cadena}`. Dónde cadena es la frase, y lo importante es utilizar **${# }**. Esto nos indica el número de caracteres contenidos en la frase.  

Para extraer el primer elemento de una cadena de texto se puede hacer de la siguiente forma: `primercaracter=${cadena:0:1}`. Dónde cadena contiene la frase, el *0* indica la primera posición de array y el *1* indica el número de caracteres que se quiere extraer.

Lo importante es utilizar **${ :nºinicio:nº de caracteres}**.  

Para quitar el primer elemento de una cadena de texto se puede hacer, `sinprimcarac={$cadena:1}`, dónde indicamos el segundo elemento, pero no se especifica el final o número de caracteres.  

Se puede hacer que la cuenta sea desde la derecha hacia la izquierda (desde el final hacia el principio), para ello se debe especificar un número negativo, por ejemplo: `final=${: -8}`, dónde el número *-8* indica la cuenta de caracteres desde el final, luego viene un *espacio* (es importante que esté) seguido de los dos puntos.  

Para descubrir la posición de un determinado carácter, `posicion=$(expr index "$cadena" "$letra")`, dónde **expr index** nos indica el índice (posición) de la letra en la cadena. Devuelve *-1* cuando la letra a buscar no se encuentra en la cadena de textos.  

Para recorrer carácter a carácter utilizaremos un bucle *for* de la siguiente forma: `for (( pos=0; pos<$final; pos++ ))`

Cambiar la primera ocurrencia de una subcadena en una cadena: `echo ${cadena/vieja/nueva}`  

Cambiar todas las ocurrencias de una subcadena en una cadena: `echo ${cadena//vieja/nueva}`  

Cambiar una subcadena en una cadena sólo si está al principio: `echo ${cadena/#vieja/nueva}`  

Cambiar una subcadena en una cadena sólo si está al final: `echo ${cadena/%vieja/nueva}`  

---

## Sección 4 - Comparar números

### Aleatoriedad con shuf

La variable de entorno que nos devuelve un número aleatorio es: `$RANDOM`.  

La variable que sí que trabaja con un mayor nivel de aleatoriedad que la anterior es: `shuf`. Trabaja con *listas* con la indicación `-e`, también podemos indicar el número de repeticiones a devolver con la opción `-r`.

Ejemplo: `shuf -e 1 2 3 4 -r -n`. Dónde el `-n` indica de las opciones dispoibles, cuandtas debe tener en cuenta.  

Para trabajar en un rango de números, por ejemplo de 5 a 10, lo indicamos así: `shuf -i 5-10`. Para el caso, en que se desee un número aleario del grupo anterior, se debe añadir `-n 1`.  

### Comparar números en Shell Script

Para compara dos números, utilizaremos el comando: `if [ $num1 -eq $num2 ] then else fi`.  

Fijarse que se utiliza `-eq` en lugar de los iguales usados para las cadenas.  

Para comparar si dos números no son iguales, se debe poner el modificador `-ne` (not equal).  

Para comparar si un número es mayor o igual que otro número, utilizar el modificador `-ge`.  

Para comparar si dos números es menor o igual que otro número, utilizar el modificador `-le`.  

Para comparar si un número es mayor (estricto) que otro número, utilizar el modificador `-gt`.  

Para comparar si un número es menor (estricto) que otro número, utilzar el modificador `-lt`.  

---

## Sección 5 - Estructuras While y Until

### Estructura While<

El bucle **while** repite una serie de instrucciones mientras la condición es *verdadera*. La instrucción se escribe, `while [ ... ] do ... done` con corchetes y espacios.  

### Estructura Until

El bucle **until** repite una serie de instrucciones mientras la condición es *falsa*. La instruccion se escribe, `until [ ... ] do ... done` con corchetes y espacios.

---

## Sección 6 - Leer datos por teclado

### Leer datos por teclado en Shell Script

El comando `read` permite leer datos por teclado. Seguido por el nombre de una variable sin el símbolo `$`. En caso de no poner un nombre, **Bash** lo que se introduce por teclado se almacena de forma automática en la variable `$REPLY`.  

Existe la posibilidad de poner el texto de la pregunta seguido de `read` añadiendo el modificador `-p` y entrecomillando el texto. Ejemplo: `read -p "Intro tu nombre"`.  

Los modificadores que se pueden utilizar son: `-d` para indicar una letra y el intro se ejecuta automáticamente, `-n` para indicar número de caracteres, `-t` para indicar el tiempo, `-p` para indicar la frase.

---

## Sección 7 - Mejorando los scripts

### Uso de exit en shell script

El comando `ls` devuelve 0, 1 ó 2 según si existe o no, lo que se está buscando. Se puede usar el comando `ls --help` para ver la ayuda.

Con la variable `echo $?` se almacena el resultado anterior, es similar al **ANS** de la calculadora.

### Redirecciones de comandos I

Tomar un fichero como entrada de un comando: **comando < fichero**.

Dirigir la salida de un comando a un fichero: **comando > fichero**. Borra el contenido del fichero. Un ejemplo de uso, sería: *cat fichero.txt > fichero2.txt* donde se copia el contenido del archivo primero en el segundo, sin mostrarlo por pantalla.

Añadir la salida de un comando a un fichero: **comando >> fichero**. Añade al contenido del fichero, la salida del comando. Con la opción *>>* el contenido del archivo no se pierde, se añade a continuación.

Leer del *fichero1* e insertar la salida del comando en *fichero2*: **comando < fichero1 > fichero2. Lee y escribe. Ejemplo: *head < fichero1.txt >> fichero2.txt*.

Leer del teclado en lugar de un fichero: **cat << ETIQUETA**. Recibe texto por teclado hasta que recibe *ETIQUETA*.

Leer del teclado en lugar de un fichero guardado en un fichero: **cat << ETIQUETA > fichero**. Con esta opción, podemos escribir por teclado hasta que escribimos la etiqueta, en ese momento, se crea el archivo y guarda el contenido.

Leer del teclado en lugar de un fichero añadiendo a un fichero: **cat << ETIQUETA >> fichero**.

### Redirecciones de comandos II

El comando `convert` tiene multitud de opciones, por ejemplo, para listar colores podemos utilizar la opción: **convert -list color**. Devuelve el nombre y el código *sRGB*.

El comando `cut` es un delimitar, se puede usar para determinar condición de límite, por ejemplo: **convert -list color | cut -d " " -f 1**. Para que seleccione un color aleatorio: **convert -list color | cut -d " " -f 1 | shuf -n 1**. En los ejemplos anteriores se ha hecho uso de la barra vertical **|**, signigica que la salida de un operando, sirve de entrada para el siguiente.

Un ejemplo de utilización, para la creación de un archivo con el contenido de las comillas simples: **echo 'convert -list color | cut -d " " -f 1 | shuf -n 1' > randomcolor.sh**. Ahora, para convertirlo en un comando de ejecución, escribir **chmod +x randomcolor.sh**. Tras lo cual, se puede ejecutar, obteniendo una salida aleatorio con el nombre del color.

### Redirecciones de error

Redirigir la salida de error a un fichero: **comando 2> fichero**. Sobreescribe el fichero si existe.

Añadir la salida de error a un fichero: **comando 2>> fichero**.

Mostrar un mensaje de error: `echo $mensaje >&2`.

La opción `echo $?` devuelve un número *0* si el comando ejecutado antes, se cumple, en caso contrario devuelve un *2*.

### Ejemplos redirecciones de error

El uso del comando `which` devuelve la ruta del ejecutable de otro comando pasado como argumento.

Un ejemplo de uso, dónde se redirige la salida: `which convert 2>&1 > /dev/null`. Y para visualizar el valor numérico: `echo $?`.

### Internacionalizar mensajes

Con el comando `echo $LANG` devuelve la configuración de idioma de la consola *(es_ES.UTF-8).

### Parámetros posicionales en Shell Script

Pasar parámetros al fichero al ejecutarlo. Para ello en los comandos usador en el archivo debe aparecer **$1** (parámetro posicional). Entonces en la ejecución **./archivo texto extra** la variabla *$1* recibe el texto *texto* y no el texto *extra*. En cambio, si se entrecomilla, si que se entiende como un único parámetro.

El *parámetro posicional* `$0` es el propio nombre del archivo que se está ejecutando. El *parámetro posicional* `$1` es el primer texto recibido, y así sucesivamente para los *$ números* siguientes.

Con `$@` ó `$*` muestra todos los parámetros recibidos. En cambio con `$#` no indica el número de parámetros recibidos.

---

## Sección 8 - Funciones

### Funciones en Shell Script

Librerías (bibliotecas), conjunto de instrucciones empaquetadas en funciones. Cada función tiene un nombre, que permite su llamada para utilizarla.

### Funciones sin parámetros ni retornos en Shell Script

La palabra reservada es `function` seguido del nombre de la función y paréntesis.
Ejemplo: `function mifuncion(){ }`. Es una función que no recibe ningún parámetro y no devuelve ninguno.

Para realizar la llamada a la función, basta con usar el nombre de la misma sin los paréntesis finales.

### Funciones con parámetros en Shell Script

La función que recibe parámetros se usa el numeral **$#**. El primer parámetro es `$1`, para el segundo es`$2` y así sucesivamente.

Para indicar el contenido de todos los parámetros recibidos se usa **$@**.

Es útil para usar en un bucle `for param in "$@"` y recorrer así todos los parámetros recibidos.

### Funciones con parámetros en scripts con parámetros

Al ejecutar un script, se le pasan los parámetros, ejemplo: *./funciones.sh Conjunto de parámetros*.

Simplemente se usa `$1` para el primer `$2` para el segundo, así sucesivamente. Para el número de parámetros `$#` y para todos los parámetros `$@`. Se coloca fuera de cualquier función en el código.

### Recibir en variables el resultado de funciones

El comando `shuf -i 0-1` devuelve un número aleatorio el 0 ó el 1. Para guardarlo en una variable en una función, `$(shuf -i 0-1)`. Entonces, dentro del código, podemos asociar el nombre de una función a una variable, almacenando la variable, el resultado de la función.

---

## Sección 9 - Operaciones Matemáticas

### Operaciones matemáticas con doble paréntesis

Para las operaciones con doble paréntesis se debe poner el símbolo de dolar antes de los mismos, de la siguiente forma `$((nº_uno+nº_dos))` para el caso de sumar dos números. Si se usan variables en lugar de números, quedaría de la siguente forma `$(($numA+$numB))`.

- `$(($num1+$num2))`    *Suma*
- `$(($num1-$num2))`    *Resta*
- `$(($num1*$num2))`    *Multiplica*
- `$(($num1/$num2))`    *Divide*
- `$(($num1%$num2))`    *Resto o Módulo*
- `$(($num1**$num2))`   *Potencia*

### Incremento y decremento con doble paréntesis

- `$((num1++))` *Asigna y suma la unidad*. Al asignar no se pone **$**.
- `$((num2--))` *Asigna y resta la unidad*. Al asignar no se pone **$**.
- `$((++num1))` *Suma la unidad y asigna resultado*. Al asignar no se pone **$**.
- `$((--num1))` *Resta la unidad y asigna resultado*. Al asignar no se pone **$**.

### El orden de operaciones PEMDAS

PEMDAS. Paréntesis, Exponentes, Multiplicación, División, Suma (Addition), Resta (Subtraction).

Ejemplo: `$((5-2*3)) = -1`.
Ejemplo: `$(((5-2)*3)) = 9`.
Ejemplo: `$((5-2**3)) = -3`.

### Operaciones con Let

El comando **let** permite asociar a una variable el resultado de una operación.

Ejemplo de utilización de la variable *let*: `let suma=num_a + num_b`. Fijarse en que no se coloca el símbolo de dolar antes del nombre de las variables. Se puede usar el símbolo de dolar antes de las variables de los operandos, funciona igual `let suma=$num_a+$num_b`. En esta segunda opción no puede haber espacios entre los operandos y el signo de operación.

La operación de división */* siembre devuelve un número *entero*. No se puede dividir por cero, daría un error.

### Operar y asignar con let y dobles paréntesis

Contracción de operandos, ejemplo: `((num+=10))` para sumar el diez a la variable. Otra opción es utilizando **let**: `let = num+=5` donde se suma cinco y se asigna a la variable. Se puede utilizar con el resto de los operandos.

### Operaciones matemáticas con expr

El comando **expr** está en desuso (depricated). La forma de usar *expr* es como se indica en el siguiente ejemplo: `suma=$(expr $num1 + $num2)` donde se tiene que poner obligatoriamente un espacio entre la variable y el operando. Si no dejamos los espacios lo que realiza, es una concatenación como en las cadena.

La potencia no se puede realizar y la multiplicación se debe usar anteponiendo la barra invertida al signo de multiplicar **\***.

### Operaciones matemáticas con bc

El comando **bc** permite realizar operaciones matemáticas sencillas y complejas.

Permite realizar operaciones con decimales. Para usar *bc* se debe utilizar las *tuberías* con el comando **|**, redirigiendo la salida hacia *bc*. Ejemplo de uso: `suma=$(echo $numA+$numB | bc)`.

Para realizar la división con decimales, se deben indicar varios parámetros, tales como la escala **scale=nº decimales;** indicando el número de decimales. Ejemplo, `$(echo "scale=2;$num1/$num2" | bc)`.

La potencia (exponente) no se escribe con doble asterisco. Se debe usar el acento circunflejo **^**. Ejemplo, `$(echo $num1^$num2 | bc)`.

### Cambio sistema de numeración con bc

Hexadecimal, binario, Octal, Decimal. Para pasar de un sistema a otro, podemos usar el comando **bc** con las instrucciones **ibase=** para indicar la base de entrada y **obase=** para indicar la base de salida. Ejemplo, `binario=$(echo "ibase=10;obase=2;$decima" | bc)`, donde el número 10 indica base decimal y el número 2 indica base binaria.

Para realizar la conversión de binario a decimal no se debe poner la instrucción **obase=10** indicando la base decimal a la salida.

- ibase=2   => corresponde con binario.
- ibase=8   => corresponde con octal.
- ibase=10  => corresponde con decimal.
- ibase=16  => corresponde con hexadecimal.

### Funciones matemáticas de bc

Las funciones matemáticas incorporadas en **bc** tal como el *seno del ángulo* en radianes usa el modificar **-l**. Ejemplo, `seno=$(echo "s($num)" | bc -l)`. Donde se especifica **s** de seno.

Para el *coseno del ángulo* en radianes se debe indicar la letra **c**.

Para el *arcotangente del ángulo* se debe usar la letra **a**.

Para el *logaritmo natural (en base e)* se debe usar la letra **l**.

Para la función de *Bessel* se debe usar la letra **j**. Esta función requiere dos parámetros de entrada. Ejemplo `$(echo "j($numA, $numB)" | bc -l)`.

Para el número *e^* elevadao, se debe usar la letra **e**.

Para la *raiz cuadrada* se debe usar la palabra **sqrt**.

Para determinar la *longitud del número de caracteres* usar la palabra **length**.

---

## Sección 10 - Bibliotecas

### Ficheros externos con funciones

Para utilizar un fichero externo dentro de un script, se debe escribir, *punto espacio y el nombre del fichero con la extensión*. Ejemplo `. externo.sh`. Con esta opción se carga el contenido del fichero dentro del script. Esta técnica es útil para usar funciones ya escritas. El archivo *externo* debe contener únicamente las funciones y las variables, no debe tener las llamadas a las funciones.

### Comentarios y documentación de scripts y funciones

Bash tiene cierta libertad en la creación de comentarios.

Para comprobar si una palabra está reservada, podemos escribir: **which math** ó **which sum** y si no devuelve nada o devuelve algo, nos indica si está libre o reservada, respectivamente. Una vez que nos indica que está reservado, podemos ver sus opciones con **sum --help**.

### Ejemplo de comentarios y documentación de funciones

Los comentarios comienzan por *#* (almohadilla). Toda la línea a continuación de este carácter no tiene efecto en la ejecución.

### Sustituir un carácter

Para la coma del decimal se debe sustituir por el punto.

### Función multiplicar

### Función lista de números y resta

Para copiar lineas de código en el editor Vim, se puede hacer colocando **:97,109 co .**, dónde estamos indicando copiar *aquí (.)* donde está el cursor, desde la línea 97 hasta la línea 109.

Otra forma de indicar la copia **:-6,. co +14**, dónde copia de la línea *-6* hasta la actual *cursor al final de la línea hasta dónde se quiere copiar* y la pegas en la línea *+14* (el cursor no estaría aquí).

Para cambiar el nombre de una variable, se puede hacer colocando **:-12,.s/multiplier/number/g**, dónde indicamos desde la línea *-12* hasta la actual *.*, cambiar *s* la palabra *multiplier* por la palabra *number*.

### Función Máximo número de decimales

### Función con decimales

### Función Potencia

Entero y entero -> sin problemas.
Aplicar decimales (**scale**) con: base decimal, potencia negativa entera o decimal.
Rederigir la salida de error si el exponente es decimal.

### Función mayor de los números

### Función menor de los números

Para copiar un número de líneas y que las peque en la posicón del cursor, indicando el intervalo de las líneas, se realiza de la siguiente forma: **:313,325 co .**.

Luego para cambiar una palabra por otro texto diferentes se puede hacer de la siguiente forma: **:-12,.s/higher/lower/g**. Dónde el significado es, desde la posición del cursor retrocediendo 12 líneas, sustituir la palabra *higher* por la palabra *lower* en todos los sitios *g*.

### Función número aleatorio

Con `echo {2..5}` generamos un rango de números entre estos dos límites. Acepta números negativos `echo {2..-5}`.

Con `convert -list colorspace` lista los espacios de colores en una línea diferente. Si usamos una *tubería* y se lo pasamos a *shuf*, `convert -list colorspace | shuf -n 1` nos devuelve un espacio de color aleatorio.

Con la función *tr " " "\n"* puedo sustituir los espacios en blando por saltos de línea. Ahora si que podemos usar la opción de un número aleatorio a partir de un vector columna: `echo {2..-5} | tr " " "\n" | shuf -n 1`.

---

## Sección 11 - Trabajar con ficheros

### Recorrer ficheros Shell Script

Para acceder a una librería, basta con poner `. math.sh` y para acceder a una función dentro de esta librería `randint 6` (número aleatorio de 0 a 6).

Para recorrer un archivo de texto, y que reconozca las tabulaciones, podemos usar la variable de entorno **IFS**. Un ejemplo, de uso sería dentro de un *while*, de la forma: `while IFS= read -r line`.

### Comprobar ficheros en Shell Script

- Comprobar si es un fichero: `if [ -f $fichero ]`.
- Comprobar si un fichero existe: `if [ -a $fichero ]`.
- Comprobar si un fichero no está vacío: `if [ -s $fichero ]`.
- Comprobar si un fichero tiene permisos de escritura: `if [ -w $fichero ]`.
- Comprobar si un fichero tiene permisos de lectura: `if [ -r $fichero ]`.
- Comprobar si un fichero tiene permisos de ejecución: `if [ -x $fichero ]`.
- Comprobar si un fichero pertenece al usuario actual: `if [ -O $fichero ]`.
- Comprobar si un fichero pertenece al grupo del usuario: `if [ -G $fichero ]`.
- Comprobar si un fichero es más nuevo que otro: `if [ $f1 -nt $f2 ]`.
- Comprobar si un fichero es más antiguo que otro: `if [ $f1 -ot $f2 ]`.
- Comprobar si es un directorio: `if [ -d $variable ]`.

---

## Sección 12 - Estructura Case

### Estructura case en Shell Script I

Se usa la palabra reservada **case**. La estructura completa sería, tal que:

```bash
case $REPLY in
    1)
        echo Ha elegido uno.
        ;;
    2)
        echo Ha elegido dos.
        ;;
    *)
        echo Opción incorrecta.
esac
```

Algunos detalles de esta estructa, son que se usa doble *;;* para finalizar cada una de las opciones. Otro detalle es que se utiliza un sólo paréntesis de cierre en el inicio  de la opción. Para finalizar este bloque condicional se escribe la palabra *case* al reveés **esac**. Para recoger un valor cualquiera se utiliza el *asterisco* como última opción.

### Estructura case en Shell Script II

Se pueden añadir rangos en una de las opciones seleccionadas. Teniendo en cuenta que sólo captura un carácter: es correcto *[6-9]* no es correcto *[6-10]* el diez lo interpreta como el uno. Para que interprete números superiores a la decena, se pueden colocar de la forma *[6-9]|10)* indicando un nuevo patrón.

```bash
case $REPLY in
    1) echo opción uno.
        ;;
    2) echo opción dos.
        ;;
    [3-9]) echo opciones desde 3 hasta 9
        ;;
esac
```

El uso de **|** es el *OR lógico* y permite añadir más opciones de selección, por ejemplo:

```bash
case $REPLY in
    1|rojo) echo opción uno.
        ;;
    2|verde) echo opción dos.
        ;;
    [3-9]|10) echo varias opciones hasta la diez.
        ;;
esca
```

### Estructura case en Shell Script III

En el bloque **case** se aceptan utilizar funciones, tales como el uso del asterisco para la búsqueda de palabras de comienza o finalizan con determinadas letras.

Para palabras que empienzan por la letra *a* se puede comprobar `a*)`.

Para palabras que terminan por la letra *o* se puede comprobar `*o)`.

También se puede utilizar el símbolo de interrogación *?*, sustituye un carácter del interior de la palabra. Ejemplo *ca?ma)*.

### Estructura case en Shell Script IV

Se crea un programa que genera números aleatorios. En el **case** se utiliza como condición el *1*. En cada una de las opciones, se utiliza funciones con condicional usando el símbolo *<>* y no la abreviatura correspondiente.

### Uso de select y case en Shell Script

La instrucción **select** nos permite seleccionar una opción. Esta estructura de código requiere del uso de la palabra reservada **break** para finalizar una de las opciones.

### Uso de case con parámetros no posicionales en Shell Script

Utilización de una función en el código dentro de un *case*.

### Uso de arrays en Shell Script I

Un *array* se conforma `rgb=(rojo verde azul)` sin comillas ni coma de separación. Para saber el número de elmentos de un array se usa el arroba dentro de los corchetes de la forma `${#rgb[@]}`. Para acceder al primer elemento del array `${rgb}`. Para acceder a un elemento diferente al primero, se usa `${rgb[2]}` estaríamos accediendo al tercer elemento.

Si al *array* a uno de los elementos se añade la barra invertida \ el siguiente elemento no lo tiene en cuenta como tal, sino como parte de uno.

Para unir dos arrays:

```bash
    rgb=(rojo verde azul)
    cmy=(cian magenta yellow)
    colores=("${rgb[@]}" "${cmy[@]}")
```

### Uso de arrays en Shell Script II

Para añadir un elemento al inicio de un array: `color=(blanco "${color[@]}")`.  
Para añadir un elemento al final de un array: `color=("${colores[@]}" rosa)`.  
Para visualizar a partir de un elemento en medio de un array: `${color[@]:3}`.  
Para visualizar desde un elemento hasta otro del array: `${color[@]:0:5}`.  
Para añadir un elemento en el centro del array: `color=("${color[@]:0:5}" violeta "${color[@:5]}")`.

---

## Sección 13 - Variables de entorno y depuración de scripts

### Uso de las variables de entorno en Shell Script

Las variabelse de entorno se escriben en mayúsculas, por ejemplo `echo $PWD`. También para `echo $HOME` ó para `echo $USER`.

Para listar todas las variables de entorno, que tiene el usuario, `printenv`.

### Depuración en Shell Script

Se puede ejecutar un programa desde la consola escribiendo `bash bucledepuracion.sh`. Tenemos la posibilidad de pasarle modificadores tal como `bash -x bucledepuracion.sh` para expadir las variables, con `-v` no expande las variables y `-e` ejecuta hasta que en algún momento `$?` deja de ser cero.

El modificador que queremas, se puede colocar en el encabezado del programa `#!/bin/bash -e`. El modificador **-e**  nos devuelve el resultado hasta que se detiene por el error. Se pueden combinar dos modificadores juntos tal como, **-ex**. En lugar de los modificadores anteriores se puede utilizar **-ev** devuelve las instrucciones y la ejecución parando en el momento dónde encuentra el fallo.

---

## Sección 14 - Proyectos crear ficheros y trabajar con imágenes

### Presentación segunda parte del curso

Herramienta que permite crear código en pantalla o en fichero.  
Herramienta que automatiza tareas con ficheros de imágenes.  

### Enunciado ejercicio crear fichero

Programar un script que reciba un fichero como parámetro. Si el fichero es de texto y existe, abra el editor. Si no existe y su extensión es .sh cree el fichero con el *shebang* y permisos de ejecución y abra el editor por defecto.

### Resolución ejercicio crear fichero I

Para comprobar si una palabra está reservada por el sistema, se puede ejecutar `which svi` para determinar si la palabra *svi* está escogida.

Crear un script que recibe un fichero y que permite generar el `#!/bin/bash` del inicio de los códigos en BASH.

### Resolución ejercicio crear fichero II

### Resolución ejercicio crear fichero III

### Anotaciones para los ejecicios

### Anotaciones dvi

Anotaciones previas a las codificaciones. Crear fichero *anotaciones.md* que permmite exportar el contenido y el fichero *funciones.md* . En consola `vim anotaciones.md funciones.md -O`. El modificador *-O* permite pantalla vertical.

### Anotaciones funciones I

El comando **mdless** de *Markdown* permite ver contenido de un archivo en consola.

### Anotaciones funciones II

## Sección 15 - Uso de gettext

### Uso de gettext I

Para mostrar los mensajes de los *srcipt* en distintos idiomas.

Escribir texto traducible `gettext "texto a traducir"`. Con el uso de `echo &(gettext "texto a traducir")`.

Crear directorio con traducciones: `mkdir locale/es/LS_MESSAGES`.

Crear fichero, todo en la misma línea: `xgettext -o fichero.pot --from-code=UTF-8 script.sh`.

Para compilar las traducciones: `msgfmt -v fichero.pot -o fichero.mo`.

Con el comando `echo $LANG` nos muestra el idioma, por ejemplo: *es_ES.UTF-8*.

Ejemplo de utilización del comando `gettext` con el modificador `-s` para imprimir texto y realizar salto de línea.

```bash
#!/bin/bash
gettext -s "Hola mundo"
```

Se crea un directorio para almacenar los ficheros traducidos en los diferentes idiomas, en la ruta: `mkdir -p locale/es/LC_MESSAGES`.

EL fichero se crea: `xgettext -o prueba-gettext.pot --from-code=UTF-8 prueba-gettext.sh`; la extensión **.pot** es obligatorio. Para ver el contenido del fichero: `vim prueba-gettext.pm`. Donde podemos observar como se añada los textos que estén con la variable `gettext`. Las cuales servirán para la traducción.

### Uso de gettext II

Pasos a seguir para añadir las traducciones de las líneas de texto:

1. Fichero con las funciones `gettext` y el texto en un idioma.
2. Generar el fichero **.pot** con el comando `xgettext -o fichero.pot --from-code=UTF-8 script.sh`.
3. En el fichero **.pot** editar y añadir el texto en el idioma traducido.

Para crear el fichero con las traducciones (todo en la misma línea):

- `cd locale/es/LC_MESSAGES
- `msginit -i ../../../fichero.pot -l es -o fichero.po`.

Para compilar las traducciones:

- `msgfmt -v fichero.pot -o fichero.mo`.

En el script se añade:

- `export TEXTDOMAINDIR=$path/locale
- `export TEXTDOMAIND=fichero`.

### Uso de gettext III

```bash
  #!/bin/bash
  SCRIPT_PATH=$(dirname $0)
  SCRIPT_NAME=$(basename $0)
  export TEXTDOMAINDIR=$path/locale
  export TEXTDOMAIN=prueba-gettext
  echo $(gettext "Hello World.")
```

## Guardar extensión

### Enunciado ejercicio Guardar Extensión

1. Función que capture la extensión de un fichero y la guarde en un fichero de texto.
2. Función que lea ese fichero de texto y extraiga la extensión.
3. Funciçon que permita modificar el fichero de configuración del lenguaje.
4. Mostrará un menú con los lenguajes disponibles. Cada lenguaje estará en un directorio.

Script que reciba:

- biblioteca.
- función.
- parámetros.
- resultado esperado.

E indique si el resultado esperado es el generado o no.

### Ejercicio "guardar extensión"

### Ejercicio "test guardar texto en fichero" I

En el archivo *testguardatext.sh*, ejecutar: `./testguardatext.sh --help`. Aparece el menaaje de texto de la función *showhelptestwritefile()*.

### Ejercicio "test guardar texto en fichero" II

### Ejercicio "test guardar texto en fichero" III

Usar: `grep function ../lib/dvilib.sh`. En caso de que no lo encuentre devuelva cadena vacía.

### Ejercicio "test guardar texto en fichero" IV

### Ejercicio "leer contenido de un fichero"

### Ejercicio "test función devuelve resultado esperado"

### Función que permite modificar el lenguaje del proyecto I

## Uso del comando grep

### Introducción al comando grep

El comando **grep --help** muestra la ayuda con las opciones disponibles. Un ejemplo de uso es `grep palabra_a_buscar fichero.txt`, permite buscar los *patrónes* en el contenido de un fichero de texto.

Para buscar las *palabras* y no los *patrones*, entonces el comando debe ser: `grep -w palabra_a_buscar fichero.txt`. Con el modificador `grep -wo palabra_a_buscar fichero.txt` no muestra únicamente la palabra y no la frase dodne se contiene esa palabra.

Para configurar la salida del comando se puede cambiar el color en el archivo `vim ~/.bashrc`, añadiendo el código siguiente:

```bash
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='ls --color=auto'
  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi
```

Otras líneas de código que se pueden añadir al fichero `~/.bashrc` son:

```bash
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
```

Si utilizamos `grep -woq palabra_a_buscar fichero.txt` y luego ejecutamos `echo $?` si devuelve el valor de **0**, es que encontró la palabra, en cambio, si devuelve **!** entonces es que no encontró la palabra.

### Modificador grep I

- Con el modificador **-n** nos numera las líneas. Ejemplo de uso: `grep -n palabra_a_buscar fichero.txt`.
- Redireccionar la salida a una *tubería*. Ejemplo: `grep -n palabra_a_buscar fichero.txt | cut -d ":" -f 1`.
- Con el modificador **-v** nos devuelve todas las líneas, excepto aquellas en las que está el patrón a buscas. Ejemplo de uso: `grep -v palabra_a_buscar fichero.txt`. Igual que antes, se puede combinar con el modificador **-v -n** para que indique las líneas.
- Con el modificador **-i** ignora palabras minúsculas o mayúsculas en el momento de realizar la búsqueda: `grep -i palabra_a_buscar fichero.txt`.

### Modificador grep II

- Para indicar el número de salidas que queramos en una coincidencia múltiple: `grep -m1 palabra_a_buscar fichero.txt`, donde el **-m 1** indica que queremos una única salida, **-m 2** indica dos salidas y así sucesivamente a medida que cambiamos el numeral.
- Con el modificador **-x** sirve para mostrar patrones que coincidan con la línea, ejemplo: `grep -x -n "" fichero.txt`, nos muestra las líneas vacías, si el texto las tiene.
- Usando tubería en el ejemplo anterior: `grep -x -n "" fichero.txt | cut -d ":" -f 1`.
- Para contar las coincidencias (en línea) se usa el modificador **-c**, ejemplo: `grep -c paralabra_a_buscar fichero.txt`. Recuerda, que devuelve el número de líneas dónde se producen las coincidencias, esto quiere decir que si hay varias coincidencias por línea, no las tienen en cuenta.
- Con el modificador **-o** devuelve todas las coincidencias del patrón: `grep -o palabra_a_buscar fichero.txt`. Alternativa al comando anterior: `grep -o palabra_a_buscar fichero.txt | wx -l`. Muestra el número de coincidencias.

### Modificador grep para extraer líneas

- Con el modificador **-A 5** nos muestra, cinco líneas posteriores a la coincidencia: `grep -A 5 palabra_a_buscar fichero.txt`.
- Con el modificador **-B 5** nos muestra, cinco líneas anteriores a la coincidencia: `grep -B 5 palabra_a_buscar fichero.txt`.
- Con el modificador **-C 5** nos muestra, cinco líneas anteriores y posteriores a la coincidencia: `grep -C 5 palabra_a_buscar fichero.txt`.
- `grep -n function dvilib.sh`. Nos muestra las funciones disponibles en el archivo, además del número de la línea.
- `grep -nx } dvilib.sh`. Nos muestra las llaves de cierre y el número.
- `grep -A33 -m1 function dvilib.sh`. Nos muestra la primera **-m1** coincidencia de **function** y las treinta y tres líneas siguientes **-A33**.

### Modificador grep para búsquedas en múltiples ficheros

- `grep en *.txt`. Nos muestra coincidencia de *en* en todos los ficheros *.txt*.
- `grep -l en *.txt`. Nos muestra los archivos donde si exista coincidencia en el patrón.
- `grep -L en *.txt`. Nos muestra los archivos donde no exista coincidencia en el patrón.
- `grep bash -r ~/`. Para realizar búsquedas recursivas en todos aquellos ficheros que exista coincidencia. Notar que primero se pone el patrón y luego el modificador.
- `grep bash -r ~/ --exclude=*.sh`. Nos muestra las coincidencias, excluyendo los ficheros indicados.
- `grep bash -r ~/ --exclude-dir=taller*`. Para excluir directorios.

## Expresiones regulares

### Introducción a las expesiones regulares

Las **expresiones regulares** son patrones que describen, expresiones de caracteres. Un patrón `grep en fichero.txt` formado por dos caracteres *en*.

- `grep -E [a-j]{3} fichero.txt`. Donde el **-E** indica que es una expresión regular extendida.
- `egrep [a-j]{3 fichero.txt}`. Es otra opción, el comando **egrep** para indicar expresión extendida.

### Uso de expresiones regulares básicas - caracteres y punto

- Si entrecomillamos, delimitamos exactamente lo que queremos buscar, por ejemplo: `grep 'en ' fichero.txt`. Notar el espacio en blanco después de los caracteres *en*.
- Si añadimos el caráter "." (punto) nos añade en la búsqueda cualquier carácter. Ejemplo: `grep 'en.a' fichero.txt`.
- Si queremos buscar el carácter "." (punto) se usa la barra invertida \ antes del punto. Ejemlo: `grep 'ten\.sh' expresiones`.

### Uso de expresiones regulares básicas - rangos

Los rangos se indican con corchetes [] en las búsquedas con **grep**.

- `grep 'ten[a-z]a' expresiones`.
- `grep 'ten[0-9]a' expresiones`.
- `grep 'ten[a-z]?a' expresiones`. Indica la interrogación *?* que puede o no estar.
- `grep -E 'ten[a-z]a' expresiones`. Si que realiza la búsqueda, al indicar **-E**.
- `grep -E 'ten[a-z]?' expresiones`. Muestra las varias opciones. Con la interrogación indica *una o más de una vez*.
- `grep 'ten[a-z]*' expresiones`. Con el símbolo \*, indica *cero o más de una vez*.
- `grep 'ten[a-z]+' expresiones`.

### Uso de expresiones regulares - corchetes

Si escribimos caracteres entre corchetes, serán los que utilice en la búsqueda.

- `grep -E 'ten[.,#]a' expresiones`.
- `grep -E 'ten[^.,#]a' expresiones`. Muestra todos menos los caractes que están después del acento circunflejo "^".
- `grep -E 'ten[.,#]{4}' expresiones`. Indica un número determinado de veces.
- `grep -E 'ten[.,#]{2,4}a' expresiones`. Rango entre 2 y 4.
- `grep -E 'ten[.,#]{4,}a' expresiones`. Indica 4 o más veces.

### Uso de expresiones regulares - clases de caracteres

- `grep -E 'ten[a-z0-9]a' expresiones`. Cualquier rango.
- `grep -E 'ten[[:alnum:]]a' expresiones`. Igual al anterior.
- `grep -E 'ten[[:alpha:]]a' expresiones`.
- `grep -E 'ten[[:blank:]]a' expresiones`. Espacios en blanco.
- `grep -E 'ten[[:digit:]]a' expresiones`. Dígitos.
- `grep -E 'ten[[:lower:]]a' expresiones`. Letras en minúsculas.
- `grep -E 'ten[[:upper:]]a' expresiones`. Letras en mayúsculas.
- `grep -E 'ten[[:punct:]]a' expresiones`. Buscar los signos de puntuación.
- `grep -E 'ten[[:graph:]]a' expresiones`. Caracteres alfabéticos.
- `grep -E 'ten[[:print:]]a' expresiones`. Igual al anterior.
- `grep -E 'ten[[:xdigit:]]a' expresiones`. Valores hexadecimales.

### Uso de expresiones regulares - cuantificadores

- `grep -E 'ten\.*a' expresiones`. Devuelve cero o más veces la coincidencia.
- `grep -E 'ten\.?a' expresiones`. Devuelve *tenaa* y *ten.a*.
- `grep -E 'ten\.+a' expresiones`. Devuelve uno o más veces la coincidencia.
- `grep -E 'ten\.{4}a' expresiones`. Devuelve *ten....a*.

### Uso de expresiones regulares - agrupaciones

- `grep -E '(ten)' expresiones`. Indica que *ten* es una única unidad.
- `grep -E '(ten){2}' expresiones`. Las búsqueda de *ten* dos veces.

### Uso de expresiones regulares - alternativas

- `grep -E '(ten|algo)' expresiones`.

### Uso de expresiones regulares - comienzos y finales

- `grep -E '^com' expresiones`. Devuelve aquellos elementos que empiezan por *com*.
- `grep -E 'i$' verbos`. Muestra lo que termina en *i*.
- `grep -E '^$' expresiones`. Comienzo y final, no tengan nada.

## Uso de AWK

### Introducción a awk

El comando **awk** permite crear **script**. La forma de ejecución es: `awk patrón fichero`.

Para ejecutar un **script** en **awk**, ejecutar: `awk -f holamundo.awk`.

Ejemplo de búsqueda con **awk**: `awk '/expresión_regular/' fichero`. Donde las expresiones regulares se escriben entre barras *//*.

Un ejemplo de búsqueda con **awk** de la palabra *tomate* con la letra *T* en mayúsculas o en *t* minúsculas: `awk '/[Tt]omate/' precios`. El resultado es la línea de texto con la palabra clave a buscar.

Búsqueda de una línea: `awk 'NR==2' precios`. Muestra el contenido de la línea solicitada.

- `awk 'NR>2' precios`. Muestra varias líneas de textos, mayores a la línea 2.
- `awk '{print $1}' precios`. Imprime el primer campo de datos del fichero.
- `awk '{print $2}' precios`. Imprime el segundo campo de datos del fichero.
- `awk '{print $NF}' precios`. *Number File*, muestra el último campo.
- `awk '{print NF}' precios`. Muestra el número de palabras que existen en cada una de las filas.
- `awk '{print FNR}' precios`. Muestra el número de líneas.
- `awk '{print length}' precios`. Muestra la longitud de cada uno de los campos.

Ejemplo holamundo.awk:

```awk
#!/bin/awk -k

BEGIN { print "Hola mundo." }
```

1. Permisos del fichero: `chmod +x holamundo.awk`.
2. Ejecución del fichero  `./holamundo.awk`.
3. Otra forma de ejecutar el fichero es: `awk -f holamundo.awk`.

### Uso de awk con NR

**NR** es el número de registro, que no líneas.

- `awk 'NR==3' precios`. Muestra la tercera línea de textos.
- `awk 'NR==3{print}' precios`. Es lo mismo a la línea anterior.
- `awk 'NR==3{next}{print}' precios`. *Salta* la siguiente instrucción.
- `awk 'NR==3{print "Esta línea está modificada";next}{print}' precios`. Cuando llega a la línea 3 imprime el texto indicado, además del contenido del fichero.
- `awk 'RS=" "' precios`. Muestra cada palabra en una fila, a partir de la primera fila.
- `awk 'ORS=" "' precios`. Muestra todo el contenido en una sola fila.
- `awk 'IRS=" "' precios`. Muestra el contenido.
- `awk '{print} ORS=";\n"' precios`. Muestra el contenido duplicado por fila.
- `awk 'NR%2{print}' precios`. Devuelve las líneas impares.