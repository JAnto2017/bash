#!/bin/bash

variable=""

echo La -z es lo mismo que la ! cuando se quiere negar.

if [ -z $variable ]
then
    echo No existe la variable o no contiene nada.
else
    echo La variable existe y contiene: $variable.
fi
