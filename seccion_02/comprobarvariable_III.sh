#!/bin/bash

echo
echo ----------------------------------------------------------
echo El -unset devuelve verdadero en caso de...
echo Las variables extendidades es con $ seguido de las llaves.
echo ----------------------------------------------------------
echo

variable=5


if [ ${variable-unset} ]
then
    echo La variable NO existe o la variable tiene algo.
else
    echo La variable existe y está vacía
fi

if [ ${variable+x} ]
then
    echo La variable existe.
else 
    echo La variable NO existe.
fi


echo ----------------------------------------------------------
echo ----------------------------------------------------------
echo ----------------------------------------------------------

if [ ${variable+x} ]
then
    if [ ${variable-unset} ]
    then
        echo la variable existe y tiene algo
    else
        echo la variable existe y está vacía
    fi
else
    if [${variable-unset} ]
    then
        echo la variable NO existe.
    else
        echo la variable SI existe.
    fi
fi
