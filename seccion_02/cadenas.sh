#!/bin/bash

# comparación de cadenas 

echo
echo -----------------------------------------------
echo ------------ Comparando cadenas ---------------
echo -----------------------------------------------
echo

cadena1="hola mundo"
cadena2="hola"

if [ "$cadena1" != "$cadena2" ]
then
    echo Son Distintos.
else
    echo Son Iguales.
fi

echo
echo -----------------------------------------------
echo

if [[ "$cadena1" =~ "$cadena2" ]]
then
    echo La cadena $cadena1 contiene la cadena $cadena2.
else
    echo La cadena $cadena2 NO contiene $cadena2.
fi

