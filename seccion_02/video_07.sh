#!/bin/bash

for letra in {A..z}
do
    if [ ! $letra == "" ]
    then
        dato=$(identify -format %$letra iuu1.jpg 2> /dev/null)
        if [ ! -z "$dato" ]
        then
            echo Con la $letra devuelve $dato
        fi
    fi
done
