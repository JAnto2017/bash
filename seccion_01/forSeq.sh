#!/bin/bash

#For con el comando seq
#seq5 (devuelve los primeros cinco números)
#seq 5 10 (devuelve los números entre el 5 y el 10)
#La instrucción $() recoge el resultado de un comando

for i in $(seq 10 31)
do
    echo "Iteración con Seq:" $i
done