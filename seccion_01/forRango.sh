#!/bin/bash

for i in {1..10..2}
do
    echo Iteración con saltos de dos en dos: $i
done

#rangos con letras y saltos
for letra in {a..z..2} 
do
    echo "Letra: " $letra
done