#!/bin/bash

contador=1

for color in rojo verde azul blanco negro

do
    echo En la iteración $contador
    echo El color es $color
    echo ""
    contador=$(( $contador + 1 ))
done