#!/bin/bash

#Sin tubería ------------------------------
for i in $(seq 0 .2 2); do
    echo "${i}"
done

#Con tubería ------------------------------
for i in $(seq 2 .2 3 | tr . ,)
do
    echo "${i}"
done