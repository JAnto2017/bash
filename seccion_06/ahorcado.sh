#!/bin/bash
#-------------------------------------------------------------------------------------
#-------------------EJERCICIO DEL AHORCADO - VÍDEO 36, 37, 38-------------------------
#-------------------------------------------------------------------------------------

palabra=$(shuf -e mesa cama lata tapa idea zinc oido odio oboe abus -n 1)
fallos=0
aciertos=0
ahorcado="ahorcado"
cadena=""

for (( guion=0; guion<${#palabra}; guion++ ))
do
    cadena=$cadena"_"
done

while [ $aciertos -lt ${#palabra} ]
do
    echo $palabra
    read -n 1 -p "Escribe una letra: "
    echo ""
    
    if [[ ! $palabra =~ $REPLY ]]
    then
        ((fallos++))
        estado=${ahorcado:0:$fallos}
        guiones=$(( ${#ahorcado} -$fallos ))
        for (( guion=0; guion<$guiones; guion++ ))
        do
            estado=$estado"_"
        done

        if [ $fallos -eq ${#ahorcado} ]
        then
            clear
            echo Has perdido el juego.
            aciertos=${#palabra}
        else
            clear
            echo Palabra: $cadena
            echo Estado: $estado
            echo Aciertos: $aciertos
            echo Fallos: $fallos
        fi
    else
        ((aciertos++))
        posicion=$(expr index $palabra $REPLY)
        ((posicion--))
        echo $posicion
        cadena=${cadena:0:$posicion}$REPLY${cadena:$posicion + 1}
        palabra=${palabra:0:$posicion}"-"${palabra:$posicion + 1}
        if [ $aciertos -eq ${#palabra} ]
        then
            clear
            echo Enhorabuena, has ganado el juego con sólo $fallos fallos.
        else
            clear
            echo Palabra: $cadena
            echo Estado: $estado
            echo Aciertos: $aciertos
            echo Fallos: $fallos
        fi
    fi
done
