#!/bin/bash

#--- Script que seleccione de forma aleatoria un número
#--- y le pide al usuario que lo adivine.
#--- En caso de que no lo acierte, le indicará al usuario
#--- si el número es mayor o menor que el indicado.
#--------------------------------------------------------------

numadivi=$(shuf -i 0-9 -n 1)
respuesta=10
intentos=0

while [ $respuesta -ne $numadivi ]
do
    read -n 1 -p "Intro un número a adivinar (0-9): " respuesta
    echo ""

    until [[ "0123456789" =~ $respuesta ]]
    do
        read -n 1 -p "Sólo se aceptan número (0-9): "
        echo ""
    done

    if [ $respuesta -gt $numadivi ]
    then
        echo El número a adivinar es menor que $respuesta.
    elif [ $respuesta -lt $numadivi ]
    then
        echo El número a Adivinar es mayor que $respuesta.
    fi

    ((intentos++))
done

echo Has acertado el número en $intentos intentos.
