#!/bin/bash

#--- Entrada, tras pulsar una sola tecla, se ejecuta el intro de forma automática.
#--- El tiempo indicado es de 2 segundos.
#---------------------------------------------------------------------------------

read -d "c" -n 1 -t 2 -p "Pulsa c para continuar tras 2 segundos se para... "

echo Hola, $REPLY.
