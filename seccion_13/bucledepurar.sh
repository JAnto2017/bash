#!/bin/bash -ev
#
# Depuración en Shell Script
# --------------------------

variable=5

for i in {1..10}
do
    echo $i - $variable
    if [ $i -eq $variable ]
    then
	((variable++))
    fi
done
