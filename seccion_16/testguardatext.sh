#!/bin/bash

# Checks if a function saves the expected result in a file.
# Parameters:
# -----------
#   -l LIBRARY
#   -f | -function FUNCTION
#   -p PARAMETER1 PARAMETER2 ...
#   -i | -file FILENAME
#   -r TEXT
#
# Print help:
#   --help
#
# TODO: Parse escape symbols in parameters.

function showhelptestwritefile(){
    # Show the help
    echo $(gettext "Checks if a function saves the expected result in a file.")
    echo $(gettext "Usage: testwritefile -l LIBRARY -f  FUNCTION -p PARAMETER [PARAMETERS] -i FILETOANALYZE -r TEXT")
    echo ""
    echo $(gettext "Print help:")
    echo -e "\t--help"
    echo ""
}

function functiontestwritefile(){
    # It checks that the received function really saves the expected text in the indicated file.
    echo ""
}

listoptionstestwritefile="--help -l -f --function -i --file -p -r"
testparameters=""


while [ "$#" -ne 0 ]
do
    case $1 in
        --help)
            showhelptestwritefile
            ;;
        -l)
            if [ ! -f $2 ]
            then
                echo $(gettext "Error. File not found.")
                exit 1
            else
                testlibrary=$2
            fi
            shift
            ;;
        -f|--function)
            testfunction=$2
            shift
            ;;
        -i|--file)
            testfile=$2
            shift
            ;;
        -r)
            testtext=$2
            shift
            ;;
        -p)
            while [[ $listoptionstestwritefile != *"$2"* ]]
            do
                testparameters=$testparameters" "$2
                shift
            done
    esac
    shift
done

if [ $testlibrary ] && [ $testfunction ] && [ $testfile ] && [ $testtext ]
then
    returngrep=$(grep "$testfunction" "$testlibrary" | grep \( | grep \)) 
    if [ "$returngrep" == "" ] 
    then
        echo $(gettext "The $testlibrary library not contain the $testfunction function")]
        exit 1
    fi

    . $testlibrary
    $testfunction $testparameters 2>&1 > /dev/null
    contentfile=$(cat $testfile)
    if [ "$contentfile" = "$testtext" ]
    then
        echo "Ok. The received function works correctly"
    else
        echo "Error. The received does not function works correctly"
    fi
else
    echo $(gettext "Error.")
    exit 1
fi
