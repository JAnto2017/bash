# Functions for the dvi command.
# ------------------------------

function putlanguage(){
    # Extracts the extension from a file and stores it in a text file.
    #
    # Parameters:
    # -----------
    #   Any file
    # Returns:
    # --------
    #   0 if the file has an extension and it has been saved correctly.
    #   1 if there is no file or the file has no extension.
    #   2 if it was unable to store the extensions.

    languagefile="lang"

    if [ ! $1 ] || [ $(expr index $1 .) -eq 0 ]
    then
        echo 1
    else
        extension=${1##*.}
        if [ -f $languagefile ] && [ -w &languagefile ]
        then    
            echo $extension > $languagefile
            if [ $? -eq 0 ]
            then
                echo 0
            else
                echo 2
            fi
        else
            echo 2
        fi
    fi
}


putlanguage hola.txt
