#!/bin/bash

numero=5


echo \$numero vale $numero
echo Con numero++ asignamos y umentamos en la unidad, resultado $((numero++))
echo Con ++numero aumentamos en la unidad y asignamos, resultado $((++numero))

echo \$numero vale $numero
echo Con numero-- asignamos y umentamos en la unidad, resultado $((numero--))
echo Con --numero aumentamos en la unidad y asignamos, resultado $((--numero))
