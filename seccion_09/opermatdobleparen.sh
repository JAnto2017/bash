#!/bin/bash

num1=16
num2=5

echo Suma de $num1 y $num2: $(($num1+$num2))
echo Resta de $num1 y $num2: $(($num1-$num2))
echo Multiplicar $num1 y $num2: $(($num1*$num2))
echo Dividir $num1 con $num2: $(($num1/$num2))
echo Resto o Módulo de $num1 con $num2: $(($num1%$num2))
echo Potenciade $num1 con $num2 es: $(($num1**$num2))
echo 
echo Restar una unidad a $num1 es $((num1--))
echo Sumar una unidad a $num1 es $((num1++))
