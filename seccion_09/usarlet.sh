#!/bin/bash

#--------------------- utilizar LET

num1=5
num2=6

let suma=$num1+$num2
let resta=$num1-$num2
let multiplicar=$num1*$num2
let dividir=$num1-$num2
let exponente=$num1**$num2

echo Los operandos no necesitan tener el símbolo de dolar.
echo "Operación con la suma $num1 + $num2 = " $suma.
echo "Operación con la resta $num1 - $num2 = " $resta.
echo "Operación con la multiplicación $num1 * $num2 = " $multiplicar.
echo "Operación con la división $num1 / $num2 = " $dividir.
echo "Operación con el exponente $num1 ^ $num2 = " $exponente.
