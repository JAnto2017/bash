#!/bin/bash

num=5

echo Al principio, la variable vale $num
#--------------------------------------------------

((num+=10))
echo Le sumamos con doble paréntesis: $num 
#--------------------------------------------------

let num+=15
echo Le sumamos 15 con let : $num
#--------------------------------------------------

let num%=3
echo Calculamos el módulo : $num
