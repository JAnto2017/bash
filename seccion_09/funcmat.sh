#!/bin/bash

#----------- Funciones matemáticas con bc -------------------

num=5
otro=3

#------------------------------------------------------------------------

seno=$(echo "s($num)" | bc -l)

echo El seno de $num es $seno radianes.

#------------------------------------------------------------------------

coseno=$(echo "c($num)" | bc -l)

echo El cose de $num es $coseno radianes.

#------------------------------------------------------------------------

arcotg=$(echo "a($num)" | bc -l)

echo El arcotangente de $num es $arcotg radianes.

#------------------------------------------------------------------------

lognatural=$(echo "l($num)" | bc -l)

echo El logaritmo natural de $num es $lognatural

#------------------------------------------------------------------------

bessel=$(echo "j($num, $otro)" | bc -l)

echo La función Bessel de $num y $otro es $bessel

#------------------------------------------------------------------------

eelevado=$(echo "e($num)" | bc -l)
echo El número e^$num es igual a $eelevado

#------------------------------------------------------------------------

raizcuadrada=$(echo "sqrt($num)" | bc -l)
echo La raiz cuadrada de $num es igual a $raizcuadrada











