#!/bin/bash

num1=5.1
num2=3.2

suma=$(echo $num1+$num2 | bc)
resta=$(echo $num1-$num2 | bc)
multiplicar=$(echo $num1*$num2 | bc)
dividir=$(echo "scale=3;$num1/$num2" | bc)

potencia_exponente=$(echo $num1^$num2 | bc)

resto=$(echo $num1%$num2 | bc)

#------------------------------------------------------------------

echo La suma de $num1 + $num2 = $suma
echo La resta de $num1 - $num2 = $resta
echo La multiplicación de $num1 y $num2 = $multiplicar
echo La división de $num1 / $num2 = $dividir
echo La potencia de $num1 elevado a $num2 = $potencia_exponente
echo El resto de $num1 % $num2 = $resto
