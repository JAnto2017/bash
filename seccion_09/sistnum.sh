#!/bin/bash

#---------------- Cambios de sistemas de numeración

decimal=59
binario=$(echo "ibase=10;obase=2;$decimal" | bc)

echo El decimal $decimal
echo Es en binario $binario

#-----------------------------------------------------------------

binario_2=1101
decimal_2=$(echo "ibase=2;$binario_2" | bc)

echo El binario $binario_2
echo Es el decimal $decimal_2

#-----------------------------------------------------------------

hexadecimal=$(echo "ibase=10;obase=16;$decimal" | bc)
echo El decimal $decimal es en hexadecimal $hexadecimal

#-----------------------------------------------------------------
binario_3=$(echo "ibase=16;obase=2;$hexadecimal" | bc)
echo El hexadecimal $hexadecimal es el binario $binario_3
#-----------------------------------------------------------------
