#!/bin/bash

#-------------------- Parámetros que se pasan a las funciones --------------------------------

echo Primer parámetro recibido en el script: $1
echo Segundo parámetro recibido en el script: $2
echo Tercer parámetro recibido en el script: $3
echo Todos los parámetros recibidos en el script: $@
echo El número de parámetros recibidos en el script es: $#
