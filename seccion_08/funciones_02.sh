#!/bin/bash

#-------------------------------- Funciones con parámetros ---------------------------------


function funcionconparametros(){
    echo Recibido $# parámetros.
    echo El primer parámetro es $1.
    echo El segundo parámetro es $2.
    echo He recibido los siguientes parámetros $@.

    for param in "$@"
    do
        echo El parámetro es $param
    done
}

funcionconparametros Es una frase de texto con parámetros
