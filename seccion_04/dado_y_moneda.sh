#!/bin/bash

#----------------------------------------------------
#---------------- Ejemplos de la sección 4 ----------
#----------------------------------------------------

dado=$(shuf -i 1-6 -n 1)

moneda=$(shuf -e cara cruz -n 1)

echo Al lanzar dado, ha salido $dado.
echo Al lanzar la moneda, ha salido $moneda.

#----------------------------------------------------

