#!/bin/bash

#---------------------------------------------------------------
# ---------- Ejercicio compara números de la sección 4 ---------
# ---------- Ejercicio compara números de la sección 4 ---------
#---------------------------------------------------------------

num1=$(shuf -i 1-10 -n 1)
num2=$(shuf -i 1-10 -n 1)

if [ $num1 -eq $num2 ]
then
    echo $num1 y $num2 son iguales.
else
    echo $num1 y $num2 no son iguales.
fi

