#!/bin/bash

# Ejercicio cambiar de la frase 'comer' por 'beber' - Vídeo 28.
# Cómo sustituir al comienzo y al final de una cadena, únicamente.


cadena="Ayer, a la hora de comer, me comí toda la comida de ayer que debería haber comido a lo largo del día de ayer."

comienzo=${cadena/#ayer/el otro día}
final=${cadena/%ayer/el otro día}

echo $comienzo
