#!/bin/bash

echo
echo --------------------------------------------------------
echo --- Determina nombre y extensión separados por punto ---
echo --------------------------------------------------------
echo

fichero="imagen.jpg"

nombre=${fichero%%.*}
extension=${fichero##*.}

echo El fichero $fichero tiene el nombre de $nombre y la extensión de $extension.

echo
echo ---------------------------------------------
echo --- Añadir al nombre del fichero palabras ---
echo ---------------------------------------------
echo

nuevonombre=$nombre"_bn""."$extension
echo El fichero $fichero se llma ahora $nuevonombre.

echo 
echo -------------------------------------------------------------------------
echo --- Para determinar la posición de un caracter en la cadena de textos ---
echo -------------------------------------------------------------------------
echo

cadena="Un texto de pruebas"
letra="p"

previo=${cadena%%$letra*}
posprevio=${#previo}

echo $cadena
echo La posición de $letra en la frase es $posprevio.

