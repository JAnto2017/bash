#!/bin/bash

echo
echo -----------------------------------------------
echo --- Recorrer carácter a carácter una cadena ---
echo -----------------------------------------------
echo

cadena="Esta frase es para recorrerla, carácter a carácter hasta uno especificado"
letra=","

position=$(expr index "$cadena" "$letra")
final=$(( $position - 1 ))

echo $cadena
sleep 1
echo Número de carácteres: $final
sleep 1

for (( pos=0; pos<$final; pos++ ))
do
    echo -n "${cadena:$pos:1}"
    sleep 1
done

