#!/bin/bash

# Ejercicio cambiar de la frase 'comer' por 'beber' - Vídeo 22.
# cada vez que encontremos la subcadena 'c' ó 'm' cambiar por 'beb'
# Otra forma de resolver el ejercicio

cadena="Ayer, a la hora de comer, me comí toda la comida que debería haber comido a lo largo del día."

cadenamodificada=""

for (( poss=0; poss<${#cadena}; poss++ ))
do
    subcadena=${cadena:$poss:3}
    

    if [ "$subcadena" = "com" ]
    then
        cadenamodificada=$cadenamodificada"deb"
        poss=$(( $poss + 2 ))
    else
        cadenamodificada=$cadenamodificada${cadena:$poss:1}
    fi
done

echo $cadenamodificada
