#!/bin/bash

echo
echo ------------------------------------------------------
echo --- Posición de un caracter en una cadena de texto ---
echo ------------------------------------------------------
echo

cadena="Esta es la prueba de determinar una lentra en un texto"
letra="p"

position=$(expr index "$cadena" "$letra")
final=$(( position - 1 ))

echo La posición de la letra $letra en la frase: $cadena.
echo Es la número: $position
echo Posición - 1: $final
