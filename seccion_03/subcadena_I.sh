#!/bin/bash

echo
echo ---------------------------------------------
echo ----- Longitud de  una cadena de texto ------
echo ---------------------------------------------
echo

cadena="Una frase completa para dividir en palabras"
longitud=${#cadena}

echo El texto: $cadena
echo Tiene una longitud de caracteres de:  $longitud

echo
echo ----------------------------------------------
echo --- Extraer caracteres de una frase ----------
echo ----------------------------------------------
echo

primcaract=${cadena:0:2}
echo Los primeros dos caracteres es [$primcaract].
