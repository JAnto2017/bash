#!/bin/bash

# Ejercicio cambiar de la frase 'comer' por 'beber' - Vídeo 22.
# cada vez que encontremos la subcadena 'c' ó 'm' cambiar por 'beb'

cadena="Ayer, a la hora de comer, me comí toda la comida que debería haber comido a lo largo del día."

salto="si"

for (( pos=0; pos<${#cadena}; pos++ ))
do
    caracter=${cadena:$pos:1}
    
    if [ "$caracter" = " " -o "$caracter" = "," -o "$caracter" = "." ]
    then
        if [ $salto ]
        then
            salto=""
        else
            echo ""
            salto="si"
        fi
    else
        echo -n $caracter
        salto=""
    fi    
done
