#!/bin/bash

# Ejercicio cambiar de la frase 'comer' por 'beber' - Vídeo 22.
# cada vez que encontremos la subcadena 'c' ó 'm' cambiar por 'beb'
# Otra forma de resolver el ejercicio

cadena="Ayer, a la hora de comer, me comí toda la comida que debería haber comido a lo largo del día."

for (( poss=0; poss<${#cadena}; poss++ ))
do

    # busca subcadena 'com' y extrae lo previo, devolvíendolo
    comienzo=${cadena%%"com"*}

    position=$(( ${#comienzo} + 3 ))
    if [ "$comienzo" != "$cadena" ]
    then
        cadena=$comienzo"deb"${cadena:$position}
    fi
done

echo $cadena
