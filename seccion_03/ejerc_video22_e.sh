#!/bin/bash

# Ejercicio cambiar de la frase 'comer' por 'beber' - Vídeo 22.
# cada vez que encontremos la subcadena 'c' ó 'm' cambiar por 'beb'
# Otra forma de resolver el ejercicio

cadena="Ayer, a la hora de comer, me comí toda la comida que debería haber comido a lo largo del día."

unaocurrencia=${cadena/com/beb}
todasocurrencias=${cadena//com/beb}

echo $cadena
echo $unaocurrencia
echo $todasocurrencias
