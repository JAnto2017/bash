#!/bin/bash

echo
echo ---------------------------------------------------------
echo --- Ejercicio concatenar palabras y aumentar variable ---
echo ---------------------------------------------------------
echo

primera="La letra número ["
segunda="] es la ("
contador=1

for letra in {a..z}
do
    frase=$primera$contador$segunda$letra")."
    echo $frase
    contador=$(( $contador + 1 ))
done
