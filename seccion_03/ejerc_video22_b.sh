#!/bin/bash

# Ejercicio cambiar de la frase 'comer' por 'beber' - Vídeo 22.
# cada vez que encontremos la subcadena 'c' ó 'm' cambiar por 'beb'
# Otra forma de resolver el ejercicio

cadena="Ayer, a la hora de comer, me comí toda la comida que debería haber comido a lo largo del día."

for (( poss=0; poss<${#cadena}*2; poss++ ))
do
    subcadena=${cadena%% *}

    if [ ${subcadena: -1} = "," -o ${subcadena: -1} = "." ]
    then
        caracterfinal=$(( ${#subcadena} - 1 ))
        echo ${subcadena:0:$caracterfinal}
    else
        echo $subcadena
    fi

    poss=$(( ${#subcadena} + 1 ))
    cadena=${cadena:$poss}
done
