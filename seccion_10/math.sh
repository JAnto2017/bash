#!/bin/bash

# Functions to perform mathematical operations more easily in Shell Script.
#--------------------------------------------------------------------------
# Date:
# Author:
# Contact:
# License: GPL
# Version:

# Most important irrational numbers
#----------------------------------
PI=3.141592653
TAU=6.28318530
PHI=1.61803398
E=2.718281828

# Checks if the received parameter is numeric.
#---------------------------------------------
# Parameters: any parameters.
# Returns: 
#   0 if the parameter is not numeric.
#   1 if the parameter is numeric.
#   2 if the parameter is numeric in Spanish format.
#
function isnumeric(){
    # ^indica el inicio, $ indica el final, ? lo recibido antes es opcional, + para varios núm.
    # (\.[0-9]+)? para indicar núm decimal menor a 1 y mayor a 0 y opcional
    if [[ $1 =~ ^(-?[0-9]+)?(\.[0-9]+)?$ ]]
    then
        echo 1
    elif [[ $1 =~ ^(-?[0-9]+)?(\,[0-9]+)?$ ]]
    then
        echo 2
    else
        echo 0
    fi
}

# Returns the sum of all received numbers.
#-----------------------------------------
# Parameters: collections of numbers.
# Returns: sum of the numbers.
#
function add(){
    # con $@ recorremos uno por uno
    # con tr cambia , por .
    addends=""
    for i in $@
    do
        if [ $(isnumeric $i) -eq 1 ]
        then
            addends=$addends"+"$i
        elif [ $(isnumeric $i) -eq 2 ]
        then
            i=$(echo $i | tr , .)
            addends=$addends"+"$i
        fi
    done
    
    addends=${addends:1}
    
    if [ "$addends" = "" ]
    then
        echo 0
    else
        result=$(echo "$addends" | bc)
        echo $result
    fi
}

# Returns the multiplication of all received numbers.
#----------------------------------------------------
# Parameters: collections of numbers.
# Returns: multiplication of the received numbers.
#
function multiply(){
     
    multiplier=""
    for i in $@
    do
        if [ $(isnumeric $i) -eq 1 ]
        then
            multiplier=$multiplier"*"$i
        elif [ $(isnumeric $i) -eq 2 ]
        then
            i=$(echo $i | tr , .)
            multiplier=$multiplier"*"$i
        fi
    done
    
    multiplier=${multiplier:1}
    
    if [ "$multiplier" = "" ]
    then
        echo 0
    else
        result=$(echo "$multiplier" | bc)
        echo $result
    fi
}

# Returns the subtraction of the first received number minus the second
# received number minus the third received number...
# ---------------------------------------------------------------------
# Parameters: collections of numbers.
# Returns: subtraction of the received numbers.
#
function subtraction(){
   
    numbers=$(onlynumbers $@)
    if [ "$numbers" = "" ]
    then
        echo 0
    else
        numbers=$(echo "$numbers" | tr " " -)
        result=$(echo "$numbers" | bc)
        echo $result
    fi
}

# Returns only the numbers received as a parameter
# ------------------------------------------------
# Parameters: parameters collection.
# Returns: A text string with the received numbers in operable format.
#
function onlynumbers(){

    number=""
    for i in $@
    do
        if [ $(isnumeric $i) -eq 1 ]
        then
            number=$number" "$i
        elif [ $(isnumeric $i) -eq 2 ]
        then
            i=$(echo $i | tr , .)
            number=$number" "$i
        fi
    done
    number=${number:1}
    echo $number
}

# Returns the numbers of decimal places of the number with the largest number
# of decimal places received as a parameter
# ---------------------------------------------------------------------------
# Parameters: any parameter.
# Returns: greater number of decimals received.
#
function maximumdecimals(){
    higher=0
    numbers=$(onlynumbers $@)
    for i in $numbers
    do
        posdot=$(expr index $i ".")
        if [ $posdot -gt 0 ]
        then
            decimal=${i##*.}
            if [ ${#decimal} -gt $higher ]
            then
                higher=${#decimal}
            fi
        fi
    done
    echo $higher
}


# Returns the division of two numbers.
# ------------------------------------
# If it receives a third parameter, it will indicate the number of decimal places.
# If not, the result will have as many decimal places as the number with the greatest number of decimal places.
# Parameters: 2 or 3 numbers.
# Returns: the division of the received numbers.
#
function division(){
    if [ $(isnumeric $1) -gt 0 -a $(isnumeric $2) -gt 0 ]
    then
        if [ $3 ]
        then
            if [ $(isnumeric $3) -gt 0 -a $(isinteger $3) -gt 0 ]
            then
                decimals=$3
            fi
        fi

        if [ ! $decimals ]
        then
            decimals=$(maximumdecimals $1 $2)
        fi

        decimals=$(abs $decimals)        
        result=$(echo "scale=$decimals;$1/$2" | bc)

        if [ ${result:0:1} = "." ]
        then
            result="0"$result
        fi

        if [ ${result:0:2} = "-." ]
        then
            result="-0"${result:1}
        fi

        echo $result
    else
        echo ""
    fi
}

# Returns 1 if the received parameter is an integer, 0 otherwise.
# ---------------------------------------------------------------
# Parameters: any value.
# Returns: 1 - parameter is integer; 0 - otherwise;
#
function isinteger(){
    if [[ $1 =~ ^-?[0-9]+$ ]]
    then
        echo 1
    else
        echo 0
    fi
}

# Returns the absolute value of the received number
# --------------------------------------------------
# Parameters: any number.
# Returns: Absolute value of number.
#
function abs(){
    if [ $(isnumeric) -gt 0 ]
    then
        if [ ${1:0:1} = "-" ]
        then
            echo ${1:1}
        else
            echo $1
        fi
    else
        echo ""
    fi
}

# Returns the power of the first number raised to the second.
# -----------------------------------------------------------
# Parameters: 2 or numbers.
# Returns: the power of the first number raised to the second.
#
function pow(){
    if [ $1 ] && [ $2 ]
    then
        if [ $(isnumeric $1) -gt 0 -a $(isnumeric $2) -gt 0 ]
        then
            base=$(onlynumbers $1)
            exponent=$(onlynumbers $2)
            if [ $(isinteger $base) -gt 0 -a $(isinteger $exponent) -gt 0 ]
            then
                if [ ${exponent:0:1} = "-" ]
                then
                    result=$(echo "scale=6;$base^$exponent" | bc)
                    if [ ${result:0:1} = "." ]
                    then
                        echo "0"$result
                    else
                        echo $result
                    fi
                else
                    echo $(echo $base^$exponent | bc)
                fi
            elif [ $(isinteger $exponent) -eq 0 ]
            then
                echo $(echo "scale=6;$base^$exponent" | bc 2> /dev/null)
            else
                echo $(echo "scale=6;$base^$exponent" | bc) 
            fi
        else
            echo ""
        fi
    else
        echo ""
    fi
}

# Return the square root of a number
# ----------------------------------
# Parameters: a number.
# Returns: the square root of first number
# Empty string if not received a positive number
#
function sqrt(){
    if [ $1 ] && [ $(isnumeric $1) -gt 0 ]
    then
        number=$(onlynumbers $1)
        if [ ${number:0:1} = "-" ]
        then
            echo ""
        else
            echo $(echo "sqrt($number)" | bc -l)
        fi
    else
        echo ""
    fi
}

# Returns the higher of the numbers received as a parameter.
# ----------------------------------------------------------
# Parameters: any parameter.
# Returns: the higher number. 0 if there'n numbers.
#
function max(){
    
    numbers=$(onlynumbers $@)
    higher=0
    for i in $numbers
    do
        if [ $(echo "$i>$higher" | bc) -eq 1 ]
        then
            higher=$i
        elif [ $(echo "$i<$higher" | bc) -eq 1 ] && [ $higher -eq 0 ]
        then
            higher=$i
        fi
    done
    echo $higher
}

# Returns the lower of the numbers received as a parameter.
# ---------------------------------------------------------
# Parameters: any parameter.
# Returns: the lower number. 0 if there are not numbers.
#
function min(){
    
    numbers=$(onlynumbers $@)
    lower=0
    for i in $numbers
    do
        if [ $(echo "$i<$lower" | bc) -eq 1 ]
        then
            lower=$i
        elif [ $(echo "$i>$lower" | bc) -eq 1 ] && [ "$lower" = "0" ]
        then
            lower=$i
        fi
    done
    echo $lower
}

# Return a random integer N such that a <= N <= b
# ------------------------------------------------
# Parameters: 1 or 2 numbers.
#   1 number: upper limit. Range: 0-$1
#   2 number: lower limit. Range: $1-$2
# Returns: a random number. 0 if there are not numbers.
#
function randint(){
    
    if [ $1 ] && [ $(isinteger $1) -gt 0 ]
    then
        if [ $2 ] && [ $(isinteger $2) -gt 0 ]
        then
            echo $(eval echo {$1..$2} | tr " " "\n" | shuf -n 1)
        else
            echo $(eval echo {0..$1} | tr " " "\n" | shuf -n 1)
        fi
    else
        echo ""
    fi

    #$(eval echo {$1..$2}) El eval es para que ejecute el echo {} y devuelve el vector de nº.
}

###############################################################################################

randint -2 -21
