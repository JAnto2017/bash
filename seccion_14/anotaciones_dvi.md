# Anotaciones dvi

dvi es un script para ayudarnos a programar más rápidos.

Utilizaremos el editor por defecto para abrir los ficheros.

Automatiza las tareas más rutinarias, como añadir el shebang, escribir documentación con su formato.

Permite añadir estructuras de código (for, if, wwhile...) en función del lenguaje de programación.  

## Mostrar ayuda
~~~
dvi --help
dvi -h
dvi help
~~~

## Crear fichero
~~~
dvi fichero
~~~

## Fichero con documentación
~~~
dvi doc "documentación del proyecto"
dvi documentation "documentación"
dvi -d "documentación"
... -p "parámetro1 parámetro2"
... parameters "par1 par2"
... par "parámetros"
... -r "valor de retorno"
... returns "valor de retorno"
... ret "valor de retorno"
~~~
