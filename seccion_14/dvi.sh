#!/bin/bash

# dvi (developer vi) is a wrapper for text editors to work faster developing code.

function showhelp(){ 
    # TODO: write the help we know what the command does.
    # echo ""
    exit 0
}
# --------------------------------------------------------------------------------
if [ ! $1 ] || [ -d $1 ] || [ ${1: -1} = "/" ]
then
    echo You must indicate a file to edit. >&2
    exit 1
elif [ -f $1 ]
then
    $EDITOR $1
else
    path=${1%/*}
    if [ ${#1} -ne ${#path} ]
    then        
        mkdir -p $path
    fi    
    extension=${1##*}
    case $extension in
        sh)
            echo "#!/bin/bash" > $1
            chmod +x $1
            ;;
    esac
    $EDITOR $1
fi
# --------------------------------------------------------------------------------
while [ $# -ne 0 ]
do
    case $1 in
        --help)
            showhelp
            ;;
    esac
    shift
done

