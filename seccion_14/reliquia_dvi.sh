#!/bin/bash

if [ $1 ]
then
    if [ ! -f $1 ] && [ ${1##*.} = "sh" ]
    then
        echo "#!/bin/bash" > $1
        chmod +x $1
    fi
    $EDITOR $1
else
    echo Debe introducir un archivo:
    exit 1
fi
