#!/bin/bash

SCRIPT_PATH=$(dirname $0)
SCRIPT_NAME=$(basename $0)

export TEXTDOMAINDIR=$SCRIPT_PATH/locale
export TEXTDOMAIN=${SCRIPT_NAME%%.*}

echo $(gettext "This is a message to translate")
