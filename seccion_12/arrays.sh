#!/bin/bash
#---------------------------------------------
# USO DE ARRAYS EN SCRIPT BASH ---------------
# --------------------------------------------

rgb=(rojo ver azul\ marino)

echo El array contiene ${#rgb[@]} elementos.
echo El primer elemento del array es: ${rgb}.
echo Los elementos del array son: ${rgb[@]}
echo El tercer elemento del array es: ${rgb[2]}.

echo Ejemplo recorriendo elemento por elemento
for i in ${rgb[@]}
do
    echo $i
done

echo Ejemplo recorriendo unidad por unidad
for i in "${rgb[@]}"
do
    echo $i
done
