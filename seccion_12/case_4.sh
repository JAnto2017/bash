#!/bin/bash
#----------------------------------------------------
# Uso de SELECT y CASE ------------------------------
# ---------------------------------------------------

echo Saluso del idioma seleccionado
echo 

select language in Español Catalán Gallego Euskera Otros
do
    case $language in
        Español | Catalán)
            echo Hola.
            break
            ;;
        Gallego)
            echo Ola. sin hache
            break
            ;;
        Euskera)
            echo Kaixo.
            break
            ;;
        Otros)
            echo Hello.
            break
            ;;
    esac
 done
