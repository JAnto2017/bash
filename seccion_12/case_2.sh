#!/bin/bash

read -p "Escriba una palabra: "
echo ""

case $REPLY in
    a*) echo La palabra empieza por A.
        ;;
    b*) echo La palabra empieza por B.
        ;;
    c*) echo La palabra empieza por C.
        ;;
    *o) echo La palabra termina por O.
        ;;
    *) echo La palabra no empieza ni por a ni por b.
        ;;
esac

