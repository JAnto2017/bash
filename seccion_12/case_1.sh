#!/bin/bash

echo ""
echo Eliga un color:
echo 1.- Rojo
echo 2.- Verde
echo 3.- Azul
echo 4.- Blanco
echo 5.- Negro

read -n 1
echo ""

case $REPLY in
    1)
        echo Ha elegido color Rojo.
        ;;
    2) 
        echo Ha elegido color Verde.
        ;;
    3)
        echo Ha elegido color Azul.
        ;;
    4)
        echo Ha elegido color Blanco.
        ;;
    5)
        echo Ha elegido color Negro.
        ;;
    *)
        echo Opción selccionada fuera del rango 1 a 5.
esac
