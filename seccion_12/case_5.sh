#!/bin/bash
#-------------------------------------------
# USO DE CASE CON PARÁMETROS NO POSICIONALES
# ------------------------------------------
# 
# Recibe una frase de varias palabras y las muestra separadas en filas

function showhelp(){
    echo Esto es la ayuda
    exit 0
}
#---------------------------------------------------------------------
while [ $# -ne 0 ]
do
    echo $1
    case $1 in
        --help)
            showhelp
            ;;
        -i)
            input=$2
            shift
            ;;
        -o)
            output=$2
            shift
            ;;
        -v)
            verbose=1
            ;;
    esac
    shift
done

echo vamos a trabajar con el fichero de entrada $input y con el fichero de salida $output.
