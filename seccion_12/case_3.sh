#!/bin/bash

aleatorio=$(shuf -i 1-9999 -n 1)

case 1 in
    $(($aleatorio < 10)))
        echo 000$aleatorio
        ;;
    $(($aleatorio < 100)))
        echo 00$aleatorio
        ;;
    $(($aleatorio < 10000)))
        echo 0$aleatorio
        ;;
    *)
        echo $aleatorio
        ;;
esac
