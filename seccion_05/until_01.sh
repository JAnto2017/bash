#!/bin/bash

#--------------------------------------------
#--------- Sección 5. Uso del UNTIL ---------
#--------------------------------------------

numero=10

until [ $numero -ge 20 ]
do
    echo $numero
    ((numero++))
done
