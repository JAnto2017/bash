#!/bin/bash

# Leemos de un fichero de texto usando while
# Devuelve el contenido del fichero línea a línea
# No devuelve las tabulaciones
# Usando la variable de entorno IFS respeta las tabulaciones.

while IFS= read -r line
do
    echo $line
done < violante.txt
