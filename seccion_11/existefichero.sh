#!/bin/bash

# Se pasa como parámetro de ejecución el nombre del fichero
# El programa comprueba si existe o no.
# El programa también comprueba la existencia o no de DIRECTORIOS.

if [ -a $1 ]
then
    echo $1 existe.
else
    echo No existe $1.
fi

if [ -f $1 ]
then
    echo $1 es un FICHERO no es un DIRECTORIO.
else
    echo No es un FICHERO $1 puede ser un DIRECTORIO.
fi

if [ -s $1 ]
then
    echo $1 No está vacío, contiene algo.
else
    echo El fichero $1 está vacío.
fi

if [ -w $1 ]
then
    echo $1 Tiene permisos de ESCRITURA usuario actual.
else
    echo $1 No tiene permisos de ESCRITURA usuario actual.
fi

if [ -r $1 ]
then
    echo $1 Tiene permisos de LECTURA usuario actual.
else
    echo $1 No tiene permisos de LECTURA usuario actual.
fi

if [ -x $1 ]
then
    echo $1 Tiene permisos de EJECUCIÓN usuario actual.
else
    echo $1 No tiene permisos de EJECUCIÓN usuario actual.
fi
