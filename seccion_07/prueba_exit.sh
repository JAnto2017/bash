#!/bin/bash

num1=4
num2=4

if [ $num1 -eq $num2 ]
then
    exit 0
else
    exit 1
fi

#------------ Si los numéros son iguales el resultado de echo $? es: 0
#------------ Si los num1 > num2 el resultado de echo $? es: 1
#------------ Si los num1 < num2 el resultado de echo $? es: 1
#------------ Si la salida de echo $? = 2 es que no existe.
