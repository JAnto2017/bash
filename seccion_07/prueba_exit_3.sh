#!/bin/bash

#-------------------------------------
#-- Ejemplos Redirecciones de error --
#-------------------------------------

which convert 2>&1 > /dev/null

if [ $? -ne 0 ]
then
    echo Comando no instalado. >&2
    exit 1
else
    echo Comando instalado.
    exit 0
fi
