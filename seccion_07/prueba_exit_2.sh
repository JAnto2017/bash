#!/bin/bash

#----------------------------
#-- Redirecciones de error --
#----------------------------

numA=2
numB=2

if [ $numA -eq $numB ]
then
    echo Prueba superada.
    exit 0
else
    echo Ha habido un error.
    exit 1
fi
