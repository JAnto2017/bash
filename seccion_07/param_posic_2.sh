#!/bin/bash

#-----------------------------------------------
#-----------------------------------------------
#--- PARÁMETRSO POSICIONALES--------------------
#-----------------------------------------------
#-----------------------------------------------
#--- $0 nombre del fichero en ejecución
#--- $1 primer parámetro
#--- $2 segundo parámetro
#--- ...
#--- $n dónde n es un número. Parámetro enésimo.
#--------------------------------

# para recorrer línea a línea los parámetros introducido
for param in "$@"
do
    echo $param
done

